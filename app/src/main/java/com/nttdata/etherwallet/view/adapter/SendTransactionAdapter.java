package com.nttdata.etherwallet.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.view.fragment.SendAdvancedTransactionFragment;
import com.nttdata.etherwallet.view.fragment.SendSimpleTransactionFragment;

/**
 * Created by Francesco on 16/11/2016.
 */

public class SendTransactionAdapter extends FragmentPagerAdapter{

    EtherAccount mAccount;

    public SendTransactionAdapter(FragmentManager fm, EtherAccount account) {
        super(fm);
        this.mAccount = account;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return SendAdvancedTransactionFragment.newInstance(mAccount, null);
            case 1:
                return SendSimpleTransactionFragment.newInstance(mAccount, null);
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


}
