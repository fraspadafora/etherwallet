package com.nttdata.etherwallet.view.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.callback.ip.SettingsCallBack;
import com.nttdata.etherwallet.presenter.manage_ip.SettingsPresenter;
import com.nttdata.etherwallet.view.activity.ip.ListIpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener, SettingsCallBack{
    private static final String TAG = SettingsActivity.class.getSimpleName();

    @BindView(R.id.settingCreateNewIp)
    Button createNewIpButton;

    @BindView(R.id.settingListIp)
    Button listIpButton;

    @BindView(R.id.activity_settings)
    RelativeLayout rootView;

    private SettingsPresenter mSettingsPresenter;
    private EtherAccount account;
    private StorageManager mStorageManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle("Settings");

        mStorageManager = StorageManager.getInstance(this);
        account = mStorageManager.getSelectedEtherAccount();

        ButterKnife.bind(this);
        createNewIpButton.setOnClickListener(this);
        listIpButton.setOnClickListener(this);

        mSettingsPresenter = new SettingsPresenter(this, this);

        if(getIntent().getExtras()!= null && getIntent().getExtras().equals(R.string.changeIP)){
            showChangeLangDialog();
        }
    }

    @Override
    public void onClick(View view) {
       Log.d(TAG, "onClick: ");
       switch (view.getId()){
           case R.id.settingCreateNewIp:
               showChangeLangDialog();
               break;
           case R.id.settingListIp:
               goToListIpActivity();
               break;
       }
    }

    private void goToListIpActivity() {
        Log.d(TAG, "goToListIpActivity: ");
        try {
            startActivity(new Intent(this, ListIpActivity.class));
        }catch (Exception e){
            Log.d(TAG, "goToListIpActivity Exception"+ e);
            e.printStackTrace();
        }
    }

    public void showChangeLangDialog() {
        Log.d(TAG, "showChangeLangDialog: ");
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_setting_layout, null);
            dialogBuilder.setView(dialogView);

            final EditText ip = (EditText) dialogView.findViewById(R.id.ip_edittext_id);
            final EditText port = (EditText) dialogView.findViewById(R.id.port_edittext_id);

            dialogBuilder.setTitle(R.string.create_new_ip);
            dialogBuilder.setMessage(R.string.message_create_new_ip);
            dialogBuilder.setPositiveButton(R.string.create_ip_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (ip.getText() != null && !ip.getText().toString().equals("") && port.getText() != null && !port.getText().toString().equals("")) {
                        mSettingsPresenter.addIpToAccount(account, ip.getText().toString(), port.getText().toString());
                    } else {
                        Snackbar.make(rootView, R.string.missing_data, Snackbar.LENGTH_SHORT).show();
                    }
                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {}
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }catch (Exception e){
            Log.d(TAG, "showChangeLangDialog: ");
            e.printStackTrace();
        }
    }


    @Override
    public void showMessageAccount() {
        Snackbar.make(rootView, R.string.noAccountSelected, Snackbar.LENGTH_SHORT).show();
    }
}
