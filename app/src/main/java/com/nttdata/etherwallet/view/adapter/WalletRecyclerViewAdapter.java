package com.nttdata.etherwallet.view.adapter;

import android.net.ConnectivityManager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.util.Utility;
import com.nttdata.etherwallet.view.fragment.interaction.OnListFragmentInteractionListener;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

/**
 * {@link RecyclerView.Adapter} that can display a {@link EtherAccount} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class WalletRecyclerViewAdapter extends BaseAdapter {

    private final List<EtherAccount> mValues;
    private final OnListFragmentInteractionListener mListener;
    private ViewHolder viewHolder;

    private EtherAccount etherAccount;

    private String mAccountAddress;
    private String balance;

    public WalletRecyclerViewAdapter(List<EtherAccount> items, String accountAddress, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mAccountAddress = accountAddress;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_wallet, parent, false);
        try {
            viewHolder = new ViewHolder(view);

            etherAccount = mValues.get(position);
            final String name = etherAccount.getName();
            final String address = etherAccount.getAddress();
            if (etherAccount.getBalance() != null) {
                balance = etherAccount.getBalance();
                BigDecimal b = new BigDecimal(balance, MathContext.DECIMAL64);
                b = Utility.etherUnitConverter(0, b, EtherUnit.ether);
                viewHolder.balance_value.setText(String.valueOf(b.toPlainString()));
                viewHolder.balance_value.setVisibility(View.VISIBLE);
                viewHolder.balanceProgress.setVisibility(View.GONE);
            } else {
                viewHolder.balance_value.setVisibility(View.GONE);
                viewHolder.balanceProgress.setVisibility(View.VISIBLE);
            }

            viewHolder.accountName.setText(name);
            String prefix = address.substring(0, 2);
            String oxAddress = null;
            if (!prefix.equals("0x")) {
                oxAddress = "0x" + address;
            } else {
                oxAddress = address;
            }
            viewHolder.address.setText(oxAddress);

            if (mAccountAddress != null && etherAccount.getAddress().equals(mAccountAddress)) {
                viewHolder.active_account.setChecked(true);
            } else {
                viewHolder.active_account.setChecked(false);
            }

            View.OnClickListener ocl = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        Log.d("WalletRecycler", "onClick Name:"+name);
                        if(mListener.checkInternetConnection()) {
                            if (balance != null && balance != "") {
                                //selectAccountFromList((String) ((AppCompatTextView) v).getText());
                                selectAccountFromList(address);
                                viewHolder.active_account.setChecked(true);
                                //viewHolder.address.getText().toString();
                                mListener.setAccountSelected(etherAccount.getId());
                                mListener.itemSelected(etherAccount);
                                if (address != null && name != null) {
                                    mListener.setAccountAddressNavigationView(address, name);
                                }
                            } else {
                                mListener.showNoDetailMassage();
                                mListener.goToAccountList();
                            }
                        }else{
                            mListener.showNoInternetConnection();
                            mListener.showSelectIPDialog();
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            };
            view.setClickable(true);
            view.setOnClickListener(ocl);



            //TODO:attualmente è presente un listener solo su address, da rifattorizzare nell'activity
            /*View.OnClickListener ocl = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        Log.d(TAG, "onClick: ");
                        if(mListener.checkInternetConnection()) {
                           if (balance != null && balance != "") {
                                selectAccountFromList((String) ((AppCompatTextView) v).getText());
                                viewHolder.active_account.setChecked(true);
                                viewHolder.address.getText().toString();
                                mListener.setAccountSelected(etherAccount.getId());
                                mListener.itemSelected(etherAccount);
                                if (address != null && name != null) {
                                    mListener.setAccountAddressNavigationView(address, name);
                                }
                           } else {
                               mListener.showNoDetailMassage();
                                mListener.goToAccountList();
                            }
                        }else{
                            mListener.showNoInternetConnection();
                            mListener.showSelectIPDialog();
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            };*/

            //viewHolder.address.setOnClickListener(ocl);
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }


    static class ViewHolder {
        @BindView(R.id.account_name)
        TextView accountName;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.check_active_account)
        RadioButton active_account;
        @BindView(R.id.balance_progress)
        ProgressBar balanceProgress;
        @BindView(R.id.balance_value)
        TextView balance_value;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void selectAccountFromList(String address){
        for(int i=0; i<mValues.size(); i++){
            if(mValues.get(i).getAddress().equals(address)){
                etherAccount = mValues.get(i);
                break;
            }
        }
    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public EtherAccount getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public EtherAccount getEtherAccount() {
        return etherAccount;
    }
}