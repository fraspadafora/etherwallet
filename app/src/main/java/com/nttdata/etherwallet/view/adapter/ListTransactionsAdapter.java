package com.nttdata.etherwallet.view.adapter;

import com.google.android.gms.wallet.firstparty.GetBuyFlowInitializationTokenResponse;
import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.data_base.StorageManager;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nttdata.etherwallet.model.transaction.Transaction;
import com.nttdata.etherwallet.util.Utility;
import com.nttdata.etherwallet.util.http_util.Constants;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

/**
 * Created by Francesco on 22/11/2016.
 */

public class ListTransactionsAdapter extends BaseAdapter {
    private static final String TAG = ListTransactionsAdapter.class.getSimpleName();

    private Context mContext;
    private ViewHolder viewHolder;
    private StorageManager mStorageManager;
    private RealmList<Transaction> listTransaction;
    private boolean success = true;

    public ListTransactionsAdapter(Context context) {
        mStorageManager = StorageManager.getInstance(context);
        listTransaction = mStorageManager.getSelectedEtherAccount().getTransactionList();
        mContext = context;
    }

//    @Override
//    public void notifyDataSetChanged() {
//        listTransaction = mStorageManager.getSelectedEtherAccount().getTransactionList();
//        super.notifyDataSetChanged();
//    }

    @Override
    public int getCount() {
        return listTransaction.size();
    }

    @Override
    public Transaction getItem(int position) {

        return listTransaction.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_transaction_list, parent, false);
        viewHolder = new ViewHolder(view);

        //final Transaction tr = listTransaction.get(position);

        //-------------------------------------------------------------------------------------
        final Transaction tr = listTransaction.get(listTransaction.size() - position - 1);
        //-------------------------------------------------------------------------------------

        if(tr.isValidate()){
            viewHolder.progressBar.setVisibility(View.GONE);
            viewHolder.validateTr.setVisibility(View.VISIBLE);
        }else if(!success){
            viewHolder.progressBar.setVisibility(View.GONE);
            viewHolder.noBlock.setVisibility(View.VISIBLE);
        }

        if(tr.getTo()!=null) {
            viewHolder.toTr.setText(tr.getTo());
        }
        if(tr.getTxId() != null) {
            viewHolder.txTr.setText(tr.getTxId());
        }
        if(tr.getAmount()!=null) {
            BigDecimal amountDecimal = new BigDecimal(tr.getAmount());
            amountDecimal = Utility.etherUnitConverter(0, amountDecimal, EtherUnit.ether);
            viewHolder.ethValue.setText(String.valueOf(amountDecimal.toPlainString()));
        }

        //TODO: rifattorizzare il listener nell'activity
        View.OnClickListener ocl = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
//                createContentDialog(tr, parent.getContext());
                showChangeLangDialog(tr);
            }
        };

        //viewHolder.toTr.setOnClickListener(ocl);
        //----------------------------------------------
        view.setClickable(true);
        view.setOnClickListener(ocl);
        //----------------------------------------------
        return view;
    }

    static class ViewHolder {
        @BindView(R.id.progress_list_tr)
        ProgressBar progressBar;
        @BindView(R.id.validate_list_tr)
        ImageButton validateTr;
        @BindView(R.id.to_value_list_tr)
        TextView toTr;
        @BindView(R.id.tx_value_list_tr)
        TextView txTr;
        @BindView(R.id.eth_value_list_tr)
        TextView ethValue;
        @BindView(R.id.no_block_list_tr)
        ImageButton noBlock;
//        @BindView(R.id.amount_transaction_converter_spinner)
//        Spinner amountTransactionConverterSpinner;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void showChangeLangDialog(final Transaction tr) {
        Log.d(TAG, "showChangeLangDialog: ");
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            final View dialogView = inflater.inflate(R.layout.dialog_transaction_detail, null);
            dialogBuilder.setView(dialogView);

            final TextView gasUsed = (TextView) dialogView.findViewById(R.id.gas_value_content);
            final EditText to = (EditText) dialogView.findViewById(R.id.to_address_value_content_list);
            final EditText from = (EditText) dialogView.findViewById(R.id.from_address_value_content_list);
            final EditText txId = (EditText) dialogView.findViewById(R.id.txid_address_value_content_list);

            final ImageButton copyTx = (ImageButton) dialogView.findViewById(R.id.copyTx);
            final ImageButton copyTo = (ImageButton) dialogView.findViewById(R.id.copyTo);
            final ImageButton copyFrom = (ImageButton) dialogView.findViewById(R.id.copyFrom);

            gasUsed.setText(tr.getGas());
            to.setText(tr.getTo());
            from.setText(tr.getAddress());
            txId.setText(tr.getTxId());

            copyTx.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label",txId.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(mContext, "Copied!", Toast.LENGTH_SHORT).show();
                }
            });

            copyTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label",to.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(mContext, "Copied!", Toast.LENGTH_SHORT).show();
                }
            });

            copyFrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label",from.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(mContext, "Copied!", Toast.LENGTH_SHORT).show();
                }
            });

            dialogBuilder.setTitle(R.string.info_transaction);
            dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {}
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }catch (Exception e){
            Log.e(TAG, "showChangeLangDialog: EXCEPTION: ", e);
            e.printStackTrace();
        }
    }

//    public void createContentDialog(final Transaction tr, Context context){
//        try {
//            final AlertDialog.Builder alert = new AlertDialog.Builder(context);
//            alert.setTitle(R.string.info_transaction);
//
//            LinearLayout mainLayout = new LinearLayout(context);
//            mainLayout.setOrientation(LinearLayout.VERTICAL);
//
//            LinearLayout gasUsedLayout = new LinearLayout(context);
//            gasUsedLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            LinearLayout gasPriceLayout = new LinearLayout(context);
//            gasPriceLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            LinearLayout amountLayout = new LinearLayout(context);
//            gasPriceLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            LinearLayout txIdLayout = new LinearLayout(context);
//            gasPriceLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            final TextView gasUsedText = new EditText(context);
//            gasUsedText.setText(R.string.gas_used);
//            gasUsedText.setEnabled(false);
//            gasUsedText.setInputType(InputType.TYPE_CLASS_TEXT);
//            gasUsedText.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
//            gasUsedLayout.addView(gasUsedText);
//
//            final TextView gasUsedValueText = new EditText(context);
//            gasUsedValueText.setText(tr.getGas());
////            gasUsedValueText.setEnabled(false);
//            gasUsedValueText.setInputType(InputType.TYPE_CLASS_TEXT);
//            gasUsedValueText.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
//            gasUsedLayout.addView(gasUsedValueText);
//
//            final TextView gasPriceText = new EditText(context);
//            gasPriceText.setEnabled(false);
//            gasPriceText.setText(R.string.gas_price);
//            gasPriceText.setInputType(InputType.TYPE_CLASS_TEXT);
//            gasPriceText.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
//            gasPriceLayout.addView(gasPriceText);
//
//            final TextView gasPriceValueText = new EditText(context);
////            gasPriceValueText.setEnabled(false);
//            gasPriceValueText.setText(tr.getGasPrice());
//            gasPriceValueText.setInputType(InputType.TYPE_CLASS_TEXT);
//            gasPriceValueText.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
//            gasPriceLayout.addView(gasPriceValueText);
//
//            final EditText amountEditText = new EditText(context);
////            gasPriceValueText.setEnabled(false);
//            gasPriceValueText.setText(tr.getAmount());
//            gasPriceValueText.setInputType(InputType.TYPE_CLASS_TEXT);
//            gasPriceValueText.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
//            amountLayout.addView(amountEditText);
//
//            final TextView txIdText = new EditText(context);
//            txIdText.setEnabled(false);
//            txIdText.setText(R.string.txId);
//            txIdText.setInputType(InputType.TYPE_CLASS_TEXT);
//            txIdText.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
//            txIdLayout.addView(txIdText);
//
//            final TextView txIdValueText = new EditText(context);
//            txIdValueText.setText(tr.getTxId());
//            txIdValueText.setInputType(InputType.TYPE_CLASS_TEXT);
//            txIdValueText.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
//            txIdLayout.addView(txIdValueText);
//
//            mainLayout.addView(gasUsedLayout);
//            mainLayout.addView(gasPriceLayout);
//            mainLayout.addView(amountLayout);
//            mainLayout.setPadding(10,10,0,0);
//            alert.setView(mainLayout);
//
//            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int whichButton) {
//
//                }
//            });
//            alert.show();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
