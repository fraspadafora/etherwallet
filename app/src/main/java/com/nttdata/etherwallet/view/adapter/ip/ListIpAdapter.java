package com.nttdata.etherwallet.view.adapter.ip;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.ip_model.IP;
import com.nttdata.etherwallet.util.http_util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

/**
 * Created by Francesco on 20/12/2016.
 */

public class ListIpAdapter extends BaseAdapter {

    private static final String TAG = ListIpAdapter.class.getSimpleName();

    private Context mContext;
    private ListIpAdapter.ViewHolder viewHolder;
    private StorageManager mStorageManager;
    private List<IP> ipList;

    public ListIpAdapter(Context context, RealmList<IP> list) {
        mStorageManager = StorageManager.getInstance(context);
        mContext = context;
        convertRealmListToList(list);
    }

    @Override
    public int getCount() {
        return ipList.size();
    }

    @Override
    public IP getItem(int position) {
        return ipList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        Log.d(TAG, "getView: ");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ip_content_layout, parent, false);
        try {

            viewHolder = new ListIpAdapter.ViewHolder(view);

            if (ipList != null) {
                IP ipObject = ipList.get(position);

                if (ipObject != null) {
                    if (ipObject.getIp_address() != null) {
                        viewHolder.ip_value.setText(ipObject.getIp_address());
                    }

                    if (ipObject.getIp_port() != null) {
                        viewHolder.port_value.setText(ipObject.getIp_port());
                    }

                    if(ipObject.isSelected()){
                        viewHolder.radioButton.setChecked(true);
                        Constants.IP_SELECTED = "http://"+viewHolder.ip_value.getText().toString()+":"+viewHolder.port_value.getText().toString();
                    }else{
                        viewHolder.radioButton.setChecked(false);
                    }
//                    if(viewHolder.radioButton != null && viewHolder.radioButton.isChecked()){
//                        Constants.IP_SELECTED = "https://"+viewHolder.ip_value.getText().toString()+":"+viewHolder.port_value.getText().toString();
//                    }
                }
            }
        }catch (Exception e){
            Log.d(TAG, "getView: EXCEPTION: "+e);
            e.printStackTrace();
        }

        return view;
    }

    static class ViewHolder {
       @BindView(R.id.ip_address_value_content_list)
       TextView ip_value;

       @BindView(R.id.port_address_value_content_list)
       TextView port_value;

        @BindView(R.id.check_active_ip)
        RadioButton radioButton;

       ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public List<IP> convertRealmListToList(RealmList<IP> realmList){
        Log.d(TAG, "convertRealmListToList: ");
        ipList = new ArrayList<>();
        try {
            for (int i = 0; i < realmList.size(); i++) {
                ipList.add(realmList.get(i));
            }
        }catch (Exception e){
            Log.d(TAG, "convertRealmListToList: EXCEPTION: "+e);
            e.printStackTrace();
        }
        return ipList;
    }
}
