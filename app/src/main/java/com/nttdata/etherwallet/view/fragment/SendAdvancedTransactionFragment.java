package com.nttdata.etherwallet.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.async_task.AsyncTaskManager;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.callback.http_callback.SendTransactionCallBack;
import com.nttdata.etherwallet.presenter.http_presenter.SendTransactionPresenter;
import com.nttdata.etherwallet.util.Utility;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.view.adapter.ListTransactionsAdapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SendAdvancedTransactionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SendAdvancedTransactionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SendAdvancedTransactionFragment extends Fragment implements View.OnClickListener, SendTransactionCallBack{
    public static String TAG = "SelectWalletFragment";

    @BindView(R.id.transactionAdvancedRelative)
    RelativeLayout rootView;

    @BindView(R.id.estimate_gas)
    Button estimate_gas;

    @BindView(R.id.progress_advanced_tr)
    ProgressBar progress;

    @BindView(R.id.to_advanced)
    EditText toAdvanced;

    @BindView(R.id.amount_advanced)
    EditText amountAdvanced;

    @BindView(R.id.gas_price_advanced)
    EditText gasPriceAdvanced;

    @BindView(R.id.gas_advanced)
    EditText gasAdvanced;

    @BindView(R.id.password_advanced)
    EditText passwordAdvanced;

    @BindView(R.id.data_advanced)
    EditText data_advanced;

    @BindView(R.id.advanced_ok)
    Button advanced_ok;

    @BindView(R.id.fee_estimation)
    TextView feeEstimation;

    @BindView(R.id.qr_button_advanced)
    ImageButton qr_code_button;

    @BindView(R.id.advanced_cancel)
    Button cancelButton;

    @BindView(R.id.total_cost)
    TextView totalCost;

    @BindView(R.id.gas_price_converter_spinner)
    Spinner gasPriceConverterSpinner;

    @BindView(R.id.amount_converter_spinner)
    Spinner amountConverterSpinner;


    private boolean do_transaction;

    private HashMap<String, Object> mapInputPrametersAdvanced;
    private StorageManager mStorageManager;

    private ProgressDialog pDialog;
    private int amountSpinnerPosition;
    private int gasPriceSpinnerPosition = -1;

    private static SendAdvancedTransactionFragment fragment;
    private Activity activity;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String accountName;
    private String address;
    private String balance;

    private static EtherAccount mAccount;

    private ListTransactionsAdapter adapter;

    private static SendTransactionPresenter mSendTransactionPresenter;

    private OnFragmentInteractionListener mListener;
    private static String qr;

    public SendAdvancedTransactionFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param account Parameter 1.
     * //@param param2 Parameter 2.
     * @return A new instance of fragment DetailWalletFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SendAdvancedTransactionFragment newInstance(EtherAccount account, String qrScan) {
        fragment = new SendAdvancedTransactionFragment();
        Bundle args = new Bundle();

        mAccount = account;
        args.putString(ARG_PARAM1, account.getAddress());

        //args.putString(ARG_PARAM2, param2);

        if(qrScan != null) {
            qr = qrScan;
        }

        fragment.setArguments(args);
        return fragment;
    }

//    public void updateTo(String qr){
//        if(qr!=null){
//            toAdvanced.setText(qr);
//        }
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            //accountName = getArguments().getString(ARG_PARAM2);
            address = getArguments().getString(ARG_PARAM1);
        }

        mSendTransactionPresenter = new SendTransactionPresenter(activity, this);
//        mSendTransactionPresenter.gasPriceServerCall(address);
        mapInputPrametersAdvanced = new HashMap<>();

        mStorageManager = StorageManager.getInstance(activity);
        adapter = new ListTransactionsAdapter(activity);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_advanced_send_transaction, container, false);
        try{
            ButterKnife.bind(this, view);
            if(qr != null) {
//                EditText to = (EditText) view.findViewById(R.id.to_simple);
                toAdvanced.setText(qr);
            }

            estimate_gas.setOnClickListener(this);
            advanced_ok.setOnClickListener(this);
            cancelButton.setOnClickListener(this);
            qr_code_button.setOnClickListener(this);
            estimate_gas.setOnClickListener(this);

            gasPriceConverterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d(TAG, "onItemSelected: ");
                    try {
                        BigDecimal gasPrice = null;
                        if (gasPriceAdvanced.getText() != null) {
                            gasPrice = new BigDecimal(gasPriceAdvanced.getText().toString());
                        }

                        if(gasPriceSpinnerPosition == -1){
                            gasPriceSpinnerPosition = position;
                        }
                        EtherUnit to = Utility.getEtherUnitFromPosition(position);
                        BigDecimal result = Utility.etherUnitConverter(gasPriceSpinnerPosition, gasPrice, to);
                        gasPriceAdvanced.setText(String.valueOf(result.toPlainString()));
                        gasPriceSpinnerPosition = position;

                    }catch (Exception e){
                        Log.d(TAG, "onItemSelected: ");
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.d(TAG, "onNothingSelected: ");
                }
            });

            amountConverterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d(TAG, "onItemSelected: ");
                    amountSpinnerPosition = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.d(TAG, "onNothingSelected: ");
                }
            });

            createDialog();

            if(mAccount != null /*&& mAccount.getTransaction() != null && mAccount.getTransaction().getData()!=null*/ && mAccount.getType()!= null && mAccount.getType().equals(Constants.TYPE_LOGISTIC)){
                data_advanced.setText(mAccount.getData());
                amountAdvanced.setText(mAccount.getAmount());
                toAdvanced.setText(mAccount.getTo());

            }

            createSpinnerAmountConverter();
            createSpinnerGasConverter();

            activity.setTitle(R.string.send_transaction_title);

            //ricevo dall'activity il risultato della scansione
            String qr_Scan = getArguments().getString("qr_scan");
            if(qr_Scan!=null) {
                toAdvanced.setText(qr_Scan);
            }

            mSendTransactionPresenter.gasPriceServerCall();
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    public void createDialog(){
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage(getString(R.string.dialog_transaction_message));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void readQRCode(){
        Log.d(TAG, "readQRCode: ");
        try {
            if(mSendTransactionPresenter.checkCameraPermission(activity)) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(activity);
                scanIntegrator.addExtra("fragment_type", "advanced");
                scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                scanIntegrator.setBeepEnabled(false);
                scanIntegrator.setOrientationLocked(true);
                scanIntegrator.initiateScan();
            }else{
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

                dialogBuilder.setTitle(R.string.permissionRequest);
                dialogBuilder.setMessage(R.string.no_permission_camera);
                dialogBuilder.setPositiveButton(R.string.create_ip_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        mSendTransactionPresenter.startInstalledAppDetailsActivity(activity);
                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {}
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        }catch (Exception e){
            Log.d(TAG, "readQRCode EXCEPTION: ");
            e.printStackTrace();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            activity =(Activity) context;
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.estimate_gas){
            if(checkInputParameters()){
                startProgressbar();
                startEstimateGas();
            }else{
                Snackbar.make(rootView, R.string.missing_data, Snackbar.LENGTH_SHORT).show();
                stopProgressBar();
            }
        }
        else if(view.getId() == R.id.advanced_ok){
            if(checkInputParameters() && passwordAdvanced.getText() != null && passwordAdvanced.getText().toString() != "" && gasAdvanced.getText()!=null && gasAdvanced.getText().toString()!="") {
                if (do_transaction) {
                    if(mSendTransactionPresenter.isOnline()) {

                        try{
                            new BigInteger(gasAdvanced.getText().toString());
                        }catch(Exception e){
                            Snackbar.make(rootView, "Invalid Inputs, please check.", Snackbar.LENGTH_SHORT).show();
                            return;
                        }

                        showProgressDialog();
                        startAdvancedTransaction();
                    }else{
                        showSnackBar(getString(R.string.no_internet_connection));
                    }
                } else {
                    stopProgressDialog();
                    Snackbar.make(rootView, R.string.no_gas_calculate, Snackbar.LENGTH_SHORT).show();
                }
            }else{
                stopProgressDialog();
                Snackbar.make(rootView, "Invalid Inputs, please check.", Snackbar.LENGTH_SHORT).show();
            }
        }else if(view.getId() == R.id.qr_button_advanced){
            readQRCode();
        }
        else if(view.getId() == R.id.advanced_cancel){
            activity.onBackPressed();
        }
    }

    public void startEstimateGas(){
        Log.d(TAG, "startEstimateGasAndSendTx: ");
        String to = "";
        try {
            if (toAdvanced.getText() != null) {
                to = toAdvanced.getText().toString();
            }
//        String to = mAccount.getTo();
//        String to = "0xCC8F057bb58D8b0489878dCBDEe66AaA8576f1B6";
            String data = null;
            String amount = mapInputPrametersAdvanced.get(Constants.AMOUNT_PARAM).toString();
            String gasPrice = mapInputPrametersAdvanced.get(Constants.GAS_PRICE_PARAM).toString();
            if (mAccount != null && mAccount.getData() != null) {
                data = mAccount.getData();
            }
            mSendTransactionPresenter.nonceServerCall(address, to, amount, data);
        }catch (Exception e){
            Log.d(TAG, "startEstimateGasAndSendTx: Exception");
            e.printStackTrace();
        }
    }

    public void startAdvancedTransaction(){
        Log.d(TAG, "startAdvancedTransaction: ");
        if (mSendTransactionPresenter.getGasPrice() != null && mSendTransactionPresenter.getGasEstimation().intValue() != -1 && mSendTransactionPresenter.getNonce().intValue() != -1) {
            try {

                HashMap<String, Object> map = new HashMap<>();
                map.put(Constants.GAS_PRICE_PARAM, mSendTransactionPresenter.getGasPrice());
                map.put(Constants.DATA_PARAM, data_advanced.getText().toString());
                map.put(Constants.GAS_ESTIMATION_PARAM, mSendTransactionPresenter.getGasEstimation());
                map.put(Constants.NONCE_PARAM, mSendTransactionPresenter.getNonce());
                map.put(Constants.PASSWORD_PARAM, passwordAdvanced.getText().toString());
                map.put(Constants.TO_PARAM, toAdvanced.getText().toString());
                map.put(Constants.AMOUNT_PARAM, String.valueOf(amountAdvanced.getText().toString()));
                map.put(Constants.ACCOUNT_PARAM, mAccount);
                map.put(Constants.PASSWORD_PARAM, passwordAdvanced.getText().toString());
                map.put(Constants.ETHER_UNIT_AMOUNT, amountSpinnerPosition);
                map.put(Constants.ETHER_UNIT_GAS_PRICE, gasPriceSpinnerPosition);


                AsyncTaskManager asynkTask = new AsyncTaskManager(rootView, pDialog, mSendTransactionPresenter);
                asynkTask.execute(map);

            } catch (Exception e) {
                Log.d(TAG, "onClick error: " + e);
                Snackbar.make(rootView, R.string.error_transaction, Snackbar.LENGTH_SHORT).show();
                stopProgressDialog();
                e.printStackTrace();
            }
        } else {
            Snackbar.make(rootView, R.string.wait_gas_for_transaction, Snackbar.LENGTH_SHORT).show();
        }
    }

    public boolean checkInputParameters(){
        if(toAdvanced.getText()!=null &&
                !toAdvanced.getText().equals("") &&
                amountAdvanced.getText().toString() != null &&
                !amountAdvanced.getText().toString().equals("") &&
                gasPriceAdvanced.getText().toString() != null &&
                !gasPriceAdvanced.getText().toString().equals("")
                ){

            if(!Utility.isValidEtherAmount(amountAdvanced.getText().toString(), amountSpinnerPosition)){
                return  false;
            }

            if(!Utility.isValidEtherAmount(gasPriceAdvanced.getText().toString(), gasPriceSpinnerPosition)){
                return  false;
            }

            if(!Utility.isValidEtherAddress(toAdvanced.getText().toString())){
                return false;
            }

            /*try{
                new BigInteger(gasAdvanced.getText().toString());
            }catch(Exception e){
                return false;
            }*/

            mapInputPrametersAdvanced.put(Constants.TO_PARAM, toAdvanced.getText().toString());
            mapInputPrametersAdvanced.put(Constants.AMOUNT_PARAM, amountAdvanced.getText().toString());
            mapInputPrametersAdvanced.put(Constants.GAS_PRICE_PARAM, gasPriceAdvanced.getText().toString());
            mapInputPrametersAdvanced.put(Constants.GAS_PARAM, gasAdvanced.getText().toString());
            mapInputPrametersAdvanced.put(Constants.PASSWORD_PARAM, passwordAdvanced.getText().toString());
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void showSnackBar(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void updateAccountAfterLogisticTransaction(String operation, EtherAccount account/*, transaction tr*/) {
        Log.d(TAG, "updateAccountAfterLogisticTransaction: ");
        try {
            if (account.getType() != null && account.getType().equals(Constants.TYPE_LOGISTIC)) {
                account.setType(Constants.TYPE_WALLET);
                mStorageManager.setDataAccountFromLogistic(null, account, Constants.TYPE_WALLET);
//                mStorageManager.updateTransactionInBlock(tr, , null, account.getAddress());
                Intent intent = new Intent();
                String data = operation;
                intent.putExtra(getString(R.string.end_transaction_logistic), data);
                intent.setComponent(new ComponentName("com.nttdata.marketplace", "com.nttdata.marketplace.view.activity.ImportDeliveryServiceActivity"));
                startActivity(intent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void stopProgressDialog() {
        pDialog.dismiss();
    }

    @Override
    public void calculateFreeEstimation(BigInteger gasEstimation) {
        try {
            BigDecimal gasPriceDecimal = new BigDecimal(gasPriceAdvanced.getText().toString());
            BigDecimal feeEstDecimal = new BigDecimal(gasEstimation).multiply(gasPriceDecimal) ;
            feeEstDecimal = Utility.etherUnitConverter(0, feeEstDecimal, EtherUnit.ether);
            feeEstimation.setText(getString(R.string.feeEstimation) + feeEstDecimal.toPlainString()+" Ether");

            BigDecimal amountDecimal = new BigDecimal(amountAdvanced.getText().toString());
            amountDecimal = Utility.etherUnitConverter(amountSpinnerPosition, amountDecimal, EtherUnit.ether);
            BigDecimal totalCDecimal = feeEstDecimal.add(amountDecimal);
            totalCost.setText(getString(R.string.total_cost) + String.valueOf(totalCDecimal.toPlainString())+ " Ether");

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void checkGasEstimationValue(BigInteger gasEstimation) {
        if(gasEstimation.intValue() >= Constants.GAS_ESTIMATION_LIMIT){
            Snackbar.make(rootView, R.string.transaction_will_fail, Snackbar.LENGTH_SHORT).show();
        }else{
            Snackbar.make(rootView, R.string.estimation_complete, Snackbar.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void startTimer() {
//
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, requestCode, intent);
        if(scanningResult != null) {
            String scanContent = scanningResult.getContents();
            if (scanContent == null) {
                toAdvanced.setText("no scan result..");
            } else {
                toAdvanced.setText(scanContent);
            }
        }
    }

    public void createSpinnerGasConverter(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity, R.array.etherunit_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gasPriceConverterSpinner.setAdapter(adapter);
    }


    public void createSpinnerAmountConverter(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity, R.array.etherunit_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        amountConverterSpinner.setAdapter(adapter);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void showProgressDialog(){
        pDialog.show();
    }

    @Override
    public void updateListTransactions(boolean success) {
//        adapter = new ListTransactionsAdapter(activity);
        adapter.setSuccess(success);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateGasPriceEditText(String gasPrice) {
        gasPriceAdvanced.setText(gasPrice);
    }

    @Override
    public void updateGasAdvanced(String gas) {
        gasAdvanced.setText(gas);
    }

    @Override
    public void setDoTransaction(boolean state) {
        do_transaction = state;
    }

    @Override
    public void stopProgressBar() {
        progress.setVisibility(View.GONE);
        rootView.setAlpha(1f);
    }

    public void startProgressbar(){
        progress.setVisibility(View.VISIBLE);
        rootView.setAlpha(0.5f);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: ");
        try {
            super.onResume();
            if(qr != null) {
                toAdvanced.setText(qr);
            }
        }catch (Exception e){
            Log.d(TAG, "onResume: ");
            e.printStackTrace();
        }
    }

}