package com.nttdata.etherwallet.view.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.util.Utility;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.view.adapter.SendTransactionAdapter;
import com.nttdata.etherwallet.view.fragment.SendAdvancedTransactionFragment;
import com.nttdata.etherwallet.view.fragment.SendSimpleTransactionFragment;

import java.math.BigDecimal;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ALL")
public class MainSendTransactionActivity extends AppCompatActivity implements ActionBar.TabListener, SendSimpleTransactionFragment.OnFragmentInteractionListener, SendAdvancedTransactionFragment.OnFragmentInteractionListener {

    private static final String TAG = MainSendTransactionActivity.class.getSimpleName();

    @BindView(R.id.viewpager_transaction)
    ViewPager viewPager;

    private SendTransactionAdapter mAdapter;
    private ActionBar actionBar;
    private StorageManager mStorageManager;
    private static EtherAccount account;

    public String qrResult;

    private String[] tabs = {"ADVANCED TRANSACTION", "SIMPLE TRANSACTION"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_send_transaction);
        mStorageManager = StorageManager.getInstance(this);

        HashMap<String, Object> map = (HashMap<String, Object>) getIntent().getSerializableExtra("account");
        account = new EtherAccount();

        if(map.get(Constants.ADDRESS_PARAM) != null) {
            account.setAddress(map.get(Constants.ADDRESS_PARAM).toString());
        }

        if(map.get(Constants.NAME_PARAM) != null) {
            account.setName(map.get(Constants.NAME_PARAM).toString());
        }

        if(map.get(Constants.BALANCE_PARAM) != null) {
            account.setBalance(map.get(Constants.BALANCE_PARAM).toString());
        }

        if(map.get(Constants.ID_PRAM) != null) {
            account.setId(map.get(Constants.ID_PRAM).toString());
        }

        if(map.get(Constants.FILE_PARAM) != null) {
            account.setFile(map.get(Constants.FILE_PARAM).toString());
        }

        if(map.get(Constants.TYPE_PARAM) != null) {
            account.setType(map.get(Constants.TYPE_PARAM).toString());
        }

        if(map.get(Constants.DATA_PARAM)!=null && map.get(Constants.TO_PARAM)!=null){
            account.setData(map.get(Constants.DATA_PARAM).toString());
            account.setTo( map.get(Constants.TO_PARAM).toString());
            account.setAmount("0");
        }

        String address = account.getAddress();

        ButterKnife.bind(this);

        actionBar = getSupportActionBar();
        mAdapter = new SendTransactionAdapter(getSupportFragmentManager(), account);

        viewPager.setAdapter(mAdapter);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        //convertion from wei - to ether
        if(account.getBalance() != null) {
            BigDecimal balanceDecimal = Utility.etherUnitConverter(0, new BigDecimal(account.getBalance()), EtherUnit.ether);
            String titleBalance = String.valueOf(balanceDecimal.toPlainString());
            actionBar.setTitle(getString(R.string.balance) + " " + titleBalance);
        }

        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        for(String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (account.getType()!=null && account.getType().equals(Constants.TYPE_LOGISTIC)) {
                Intent intent = new Intent();
                String data = Constants.TRANSACTION_FAILURE;
                mStorageManager.setDataAccountFromLogistic(null, account, Constants.TYPE_WALLET);
                intent.putExtra(getString(R.string.end_transaction_logistic), data);
                intent.setComponent(new ComponentName("com.nttdata.marketplace", "com.nttdata.marketplace.view.activity.ImportDeliveryServiceActivity"));
                startActivity(intent);
            } else {
                int count = getFragmentManager().getBackStackEntryCount();

                if (count == 0) {
                    super.onBackPressed();
                } else {
                    getFragmentManager().popBackStack();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

//    public void startQrScan(){
//        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
//        scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
//        scanIntegrator.initiateScan();
//    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        Log.d(TAG, "onTabSelected: ");
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        Log.d(TAG, "onTabUnselected: ");
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        Log.d(TAG, "onTabReselected: ");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
//        Set<String> keySet = intent.getExtras().keySet();
        if(result != null) {
            if(result.getContents() == null) {
                Log.d(TAG, "onActivityResult: CANCELLED");
            } else {
                Log.d(TAG, "onActivityResult: "+  result.getContents());
                SendSimpleTransactionFragment fr = new SendSimpleTransactionFragment();
                //TODO: per un eventuale refactoring considerare l'ipotesi di separare i due fragment.
                getSupportFragmentManager().beginTransaction().replace(R.id.frameAdvancedTransactionFragment, SendAdvancedTransactionFragment.newInstance(account, result.getContents()), SendAdvancedTransactionFragment.TAG);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameSimpleTransactionFragment, SendSimpleTransactionFragment.newInstance(account, result.getContents()), SendAdvancedTransactionFragment.TAG);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }
}