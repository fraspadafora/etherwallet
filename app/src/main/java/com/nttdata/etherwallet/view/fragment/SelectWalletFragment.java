package com.nttdata.etherwallet.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.callback.http_callback.SelectFragmentCallBack;
import com.nttdata.etherwallet.presenter.http_presenter.SelectFragmentPresenter;
import com.nttdata.etherwallet.view.activity.ip.ListIpActivity;
import com.nttdata.etherwallet.view.adapter.WalletRecyclerViewAdapter;
import com.nttdata.etherwallet.view.fragment.interaction.OnListFragmentInteractionListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SelectWalletFragment extends ListFragment implements SelectFragmentCallBack{

    // TODO: Customize parameter argument names

    public static String TAG = "SelectWalletFragment";

    private static SelectFragmentPresenter mSelectFragmentPresenter;
    private static WalletRecyclerViewAdapter adapter;

    private static final String ARG_COLUMN_COUNT = "column-count";
    @BindView(android.R.id.list)
    ListView list;
    @BindView(R.id.text_no_wallet_here)
    TextView textNoWalletHere;
    @BindView(android.R.id.empty)
    CardView empty;

    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private static Context mContext;
    private Typeface nobelLightTypeface;
    private static List<EtherAccount> listAccount;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SelectWalletFragment() {
    }

    // TODO: Customize parameter initialization
    public static SelectWalletFragment newInstance(List<EtherAccount> etherAccountList, String accountAddress, OnListFragmentInteractionListener listFragmentInteractionListener) {
        Log.d(TAG, "newInstance: ");
        SelectWalletFragment fragment = new SelectWalletFragment();

        try {
            adapter = new WalletRecyclerViewAdapter(etherAccountList, accountAddress, listFragmentInteractionListener);
            listAccount = etherAccountList;

//            for (int i = 0; i < etherAccountList.size(); i++) {
//                mSelectFragmentPresenter.balanceRequest(etherAccountList.get(i).getAddress());
//            }

            fragment.setListAdapter(adapter);
        }catch (Exception e){
            Log.d(TAG, "newInstance: ");
            e.printStackTrace();
        }
        return fragment;
    }

    public void startBalanceRequest(){
        for (int i = 0; i < listAccount.size(); i++) {
            mSelectFragmentPresenter.balanceRequest(listAccount.get(i).getAddress());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_wallet_list, container, false);
        ButterKnife.bind(this, view);

        nobelLightTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/nobel-light.ttf");
        textNoWalletHere.setTypeface(nobelLightTypeface);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
        mContext = context;
        mSelectFragmentPresenter = new SelectFragmentPresenter(mContext, this);
        startBalanceRequest();
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void updateAccountList() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void serverUnreachable() {
        new AlertDialog.Builder(mContext)
                .setTitle("Server unreachable!")
                .setMessage("Do you want insert a new ip address?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mContext.startActivity(new Intent(mContext, ListIpActivity.class));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
