package com.nttdata.etherwallet.view.fragment.interaction;

import com.nttdata.etherwallet.model.EtherAccount;

public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(EtherAccount item);
        void itemSelected(EtherAccount etherAccount);
        void setAccountSelected(String id);
        void setAccountAddressNavigationView(String address, String name);

        void showNoDetailMassage();
        void showNoInternetConnection();
        void showSelectIPDialog();

        boolean checkInternetConnection();
        void goToAccountList();
}