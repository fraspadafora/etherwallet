package com.nttdata.etherwallet.view.activity.ip;

import android.app.ListActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.model.ip_model.IP;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.util.http_util.UrlServerPath;
import com.nttdata.etherwallet.view.adapter.ip.ListIpAdapter;

import io.realm.RealmList;

public class ListIpActivity extends ListActivity{
    private final String TAG = ListIpActivity.class.getSimpleName();


    private RadioButton radioChecked;

    SharedPreferences  mPrefs;

    private StorageManager mStorageManager;
    private EtherAccount account;
    private  RealmList<IP> ipList;
    public boolean inSession = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.list_ip_layout);

            mPrefs = getPreferences(MODE_PRIVATE);

            //sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

            mStorageManager = StorageManager.getInstance(this);
            account = mStorageManager.getSelectedEtherAccount();

            inSession = true;
            //TODO: inserire direttamente nell'activity label del manifest
            setTitle("IP List");

            IP defaultIp = new IP(UrlServerPath.default_ip, UrlServerPath.default_port);
            if (account != null && account.getIpList() != null) {
                ipList = account.getIpList();
            }else{
                ipList = new RealmList<>();
            }

            RealmList<IP> newIpList = mStorageManager.addDefaultIP(ipList, defaultIp);
            ListIpAdapter adapter = null;
            if (newIpList != null) {
                adapter = new ListIpAdapter(this, newIpList);
            } else {
                adapter = new ListIpAdapter(this, ipList);
            }
            setListAdapter(adapter);

        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "onCreate Exception: "+e);
        }

    }

    public RealmList<IP> setAllCheckRadioToFalse(RealmList<IP> list){
        for(int i=0;i<list.size();i++){
            mStorageManager.setIpSelected(list.get(i), false);
        }
        return list;
    }

 /*  public void storeRadioInSharedPreference(RadioButton radio){
       try {
           SharedPreferences.Editor prefsEditor = mPrefs.edit();
           Gson gson = new Gson();
           String json = gson.toJson(radio);
           prefsEditor.putString("radioChecked", json);
           prefsEditor.commit();
       }catch (Exception e){
           Log.d(TAG, "storeRadioInSharedPreference Exception "+e);
           e.printStackTrace();
       }
   }*/

    /*public RadioButton getRadioChecked(){
        try {
            Gson gson = new Gson();
            String json = mPrefs.getString("radioChecked", "");
            RadioButton obj = gson.fromJson(json, RadioButton.class);
            return obj;
        }catch (Exception e){
            Log.d(TAG, "getRadioChecked Exception "+e);
            e.printStackTrace();
            return null;
        }
    }*/

    @Override
    protected void onListItemClick (ListView l, View v, int position, long id) {
        Log.d(TAG, "onListItemClick: ");
        try {
            if (ipList != null && ipList.size() > 0) {
                ipList = setAllCheckRadioToFalse(ipList);
                RadioButton radio = (RadioButton) v.findViewById(R.id.check_active_ip);
                //radioChecked = getRadioChecked();


                if(radioChecked != null ){
                    if(radio.isChecked()){
                        radio.setChecked(false);
                        mStorageManager.setIpSelected(ipList.get(position), false);
                        //mStorageManager.setIpRadioChecked(ipList.get(position), -1);
                    }else{
                        radio.setChecked(true);
                        mStorageManager.setIpSelected(ipList.get(position), true);
                       // mStorageManager.setIpRadioChecked(ipList.get(position), position);
                    }
                    radioChecked.setChecked(false);
                    radioChecked = radio;
                }else{
                    if(radio.isChecked()){
                        radio.setChecked(false);
                        mStorageManager.setIpSelected(ipList.get(position), false);
                       // mStorageManager.setIpRadioChecked(ipList.get(position), -1);
                    }else {
                        radio.setChecked(true);
                        mStorageManager.setIpSelected(ipList.get(position), true);
                       // mStorageManager.setIpRadioChecked(ipList.get(position), position);
                    }

                    radioChecked = radio;
                }

                //storeRadioInSharedPreference(radioChecked);
                //sharedPreferences.edit().putBoolean("bntChecked", radioChecked.isChecked()).apply();
                IP ip = ipList.get(position);
                ip = mStorageManager.setIpSelected(ip, true);
                String ipAddress = ip.getIp_address();
                String port = ip.getIp_port();
                String ipSelected = UrlServerPath.http + ipAddress + ":" + port;
                Constants.setIpSelected(ipSelected);
                Toast.makeText(this, R.string.ip_selected, Toast.LENGTH_LONG).show();
            }
        }catch(Exception e){
            e.printStackTrace();
            Log.d(TAG, "onListItemClick Exception: "+e);
        }

    }


}
