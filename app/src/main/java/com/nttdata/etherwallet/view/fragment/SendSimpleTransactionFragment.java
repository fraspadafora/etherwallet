package com.nttdata.etherwallet.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.async_task.AsyncTaskManager;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.callback.http_callback.SendTransactionCallBack;
import com.nttdata.etherwallet.presenter.http_presenter.SendTransactionPresenter;
import com.nttdata.etherwallet.util.Utility;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.view.adapter.ListTransactionsAdapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SendSimpleTransactionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SendSimpleTransactionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SendSimpleTransactionFragment extends Fragment implements View.OnClickListener, SendTransactionCallBack {
    public static String TAG = SendSimpleTransactionFragment.class.getSimpleName();

    @BindView(R.id.sendTransactionSimpleRelative)
    RelativeLayout rootView;

    @BindView(R.id.to_simple)
    EditText to_simple;

    @BindView(R.id.amount_simple)
    EditText amount_simple;

    @BindView(R.id.password_simple)
    EditText password_simple;
//    @BindView(R.id.estimate_gas_button_simple)
//    Button estimate_gas_button_simple;

    @BindView(R.id.simple_ok)
    Button simple_ok;

    @BindView(R.id.progress_simple_tr)
    ProgressBar progress;

    @BindView(R.id.qr_simple_button)
    ImageButton qr_simple_button;

    @BindView(R.id.simple_cancel)
    Button buttonCancel;

    @BindView(R.id.amount_simple_converter_spinner)
    Spinner spinnerConverter;

    @BindView(R.id.fee_estimation_simple)
    TextView feeEstimation;

    @BindView(R.id.total_cost_simple)
    TextView totalCost;

    @BindView(R.id.estimate_gas_simple)
    Button estimateGasButton;

//    @BindView(android.R.id.list)
//    ListView listView;

    private SendTransactionPresenter mSendTransactionPresenter;
    private HashMap<String, Object> mapInputPrametersSimple;

    private Context mContext;

    private ProgressDialog pDialog;
    private int amountSpinnerPosition;

    private String gasPrice;

    private Activity activity;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String address;
    private static EtherAccount mAccount;
    private boolean do_transaction;

    private ListTransactionsAdapter adapter;

    private OnFragmentInteractionListener mListener;
    private StorageManager mStorageManager;

    private static String qr;

    public SendSimpleTransactionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     //* @param param1 Parameter 1.
     * @param account
     * @return A new instance of fragment DetailWalletFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SendSimpleTransactionFragment newInstance(EtherAccount account,  String qrScan) {
        SendSimpleTransactionFragment fragment = new SendSimpleTransactionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, account.getAddress());

        if(qrScan != null) {
            qr = qrScan;
        }
        if(account == null){

        }else {
            mAccount = account;
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSendTransactionPresenter = new SendTransactionPresenter(activity, this);
        mapInputPrametersSimple = new HashMap<>();

        mStorageManager = StorageManager.getInstance(activity);

        adapter = new ListTransactionsAdapter(activity);

        if (getArguments() != null) {
            address = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple_send_transaction, container, false);
        try{
            ButterKnife.bind(this, view);
//          estimate_gas_button_simple.setOnClickListener(this);
            simple_ok.setOnClickListener(this);
            qr_simple_button.setOnClickListener(this);
            buttonCancel.setOnClickListener(this);
            estimateGasButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(checkInputParametersWithoutPassword()){
                        mSendTransactionPresenter.estimateGasCall(address, to_simple.getText().toString(), Utility.etherUnitConverter(amountSpinnerPosition, new BigDecimal(amount_simple.getText().toString()), EtherUnit.wei).toBigInteger().toString(),null);
                    } else{
                        Snackbar.make(rootView, "Invalid inputs, please check.", Snackbar.LENGTH_SHORT).show();
                    }
                }
            });

            if(qr != null) {
                to_simple.setText(qr);
            }

            spinnerConverter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d(TAG, "onItemSelected: ");
                    amountSpinnerPosition = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.d(TAG, "onNothingSelected: ");
                }
            });

            createSpinnerAmountConverter();
            createDialog();
            activity.setTitle(R.string.send_simple_transaction_title);
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void createDialog(){
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage(getString(R.string.dialog_transaction_message));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mContext = context;
            mListener = (OnFragmentInteractionListener) context;
            activity = (Activity)context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void readQRCode(){
        Log.d(TAG, "readQRCode: ");
        try {
            if(mSendTransactionPresenter.checkCameraPermission(activity)) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(activity);
                scanIntegrator.addExtra("fragment_type", "simple");
                scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                scanIntegrator.setBeepEnabled(false);
                scanIntegrator.initiateScan();
            }else{
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

                dialogBuilder.setTitle(R.string.permissionRequest);
                dialogBuilder.setMessage(R.string.no_permission_camera);
                dialogBuilder.setPositiveButton(R.string.create_ip_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        mSendTransactionPresenter.startInstalledAppDetailsActivity(activity);
                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {}
                });
                AlertDialog b = dialogBuilder.create();
                b.show();

            }
        }catch (Exception e){
            Log.d(TAG, "readQRCode Exception: ");
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.qr_simple_button){
            readQRCode();
        }
        if(view.getId() == R.id.simple_ok){
            showSnackBar(activity.getString(R.string.start_transaction));
            if(checkInputParameters() && password_simple.getText() != null && password_simple.getText().toString() != "") {

                mapInputPrametersSimple.put(Constants.TO_PARAM, to_simple.getText().toString());
                mapInputPrametersSimple.put(Constants.AMOUNT_PARAM, amount_simple.getText().toString());
                mapInputPrametersSimple.put(Constants.PASSWORD_PARAM, password_simple.getText().toString());

                if(mSendTransactionPresenter.isOnline()) {
                    showProgressDialog();
                    startEstimateGasAndSendTx();
                }else{
                    showSnackBar(getString(R.string.no_internet_connection));
                }
            }else{
                stopProgressDialog();
                Snackbar.make(rootView, "Invalid inputs, please check.", Snackbar.LENGTH_SHORT).show();
            }
        }
        else if(view.getId() == R.id.simple_cancel){
            activity.onBackPressed();
        }
    }

    public void startEstimateGasAndSendTx(){
        Log.d(TAG, "startEstimateGasAndSendTx: ");
        try {
            String to = "";
            String data = null;
            if(to_simple.getText() != null) {
                to = to_simple.getText().toString();
            }
//        "0xCC8F057bb58D8b0489878dCBDEe66AaA8576f1B6" -- to
            mSendTransactionPresenter.gasPriceServerCall();
            mSendTransactionPresenter.nonceServerCall(address, to, Utility.etherUnitConverter(amountSpinnerPosition, new BigDecimal(amount_simple.getText().toString()), EtherUnit.wei).toPlainString(), data);
        }catch (Exception e){
            Log.d(TAG, "startEstimateGasAndSendTx: ");
            e.printStackTrace();
        }
    }

    public void startSimpleTransaction(){
        Log.d(TAG, "startSimpleTransaction: ");
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put(Constants.GAS_PRICE_PARAM, mSendTransactionPresenter.getGasPrice());
            map.put(Constants.GAS_ESTIMATION_PARAM, mSendTransactionPresenter.getGasEstimation());
            map.put(Constants.NONCE_PARAM, mSendTransactionPresenter.getNonce());
            map.put(Constants.PASSWORD_PARAM, password_simple.getText().toString());
            map.put(Constants.TO_PARAM, to_simple.getText().toString());
            map.put(Constants.AMOUNT_PARAM, amount_simple.getText().toString());
            map.put(Constants.ETHER_UNIT_AMOUNT, amountSpinnerPosition);
            if(mAccount.equals(null)){
                //mStorageManager = StorageManager.getInstance(mContext);
                mAccount = mStorageManager.getSelectedEtherAccount();
            }
            map.put(Constants.ACCOUNT_PARAM, mAccount);

            AsyncTaskManager asynkTask = new AsyncTaskManager(rootView, pDialog, mSendTransactionPresenter);
            asynkTask.execute(map);

        } catch (Exception e) {
            Log.d(TAG, "onClick error: " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, requestCode, intent);
        if(scanningResult != null) {
            String scanContent = scanningResult.getContents();
            if (scanContent == null) {
                to_simple.setText("no scan result..");
            } else {
                to_simple.setText(scanContent);
            }
        }
    }

    public boolean checkInputParametersWithoutPassword(){
        if(to_simple.getText()!=null &&
                amount_simple.getText() != null){

            if(!Utility.isValidEtherAmount(amount_simple.getText().toString(), amountSpinnerPosition)){
                return  false;
            }
            if(!Utility.isValidEtherAddress(to_simple.getText().toString())){
                return false;
            }

            return true;
        }else{
            return false;
        }
    }

    public boolean checkInputParameters(){
        if(to_simple.getText()!=null &&
                amount_simple.getText() != null && password_simple.getText() != null &&
                password_simple.getText().toString().length()!=0){

            if(!Utility.isValidEtherAmount(amount_simple.getText().toString(), amountSpinnerPosition)){
                return  false;
            }
            if(!Utility.isValidEtherAddress(to_simple.getText().toString())){
                return false;
            }

            return true;
        }else{
            return false;
        }
    }

    @Override
    public void setDoTransaction(boolean state) {
        startSimpleTransaction();
    }

    @Override
    public void stopProgressBar() {
        progress.setVisibility(View.GONE);
        rootView.setAlpha(1f);
    }

    @Override
    public void showSnackBar(final String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void updateAccountAfterLogisticTransaction(String operation, EtherAccount account) {
        Log.d(TAG, "updateAccountAfterLogisticTransaction: ");
        try {
            if (account.getType() != null && account.getType().equals(Constants.TYPE_LOGISTIC)) {
                account.setType(Constants.TYPE_WALLET);
                mStorageManager.setDataAccountFromLogistic(null, account, Constants.TYPE_WALLET);
                Intent intent = new Intent();
                String data = operation;
                intent.putExtra(getString(R.string.end_transaction_logistic), data);
                intent.setComponent(new ComponentName("com.nttdata.marketplace", "com.nttdata.marketplace.view.activity.ImportDeliveryServiceActivity"));
                startActivity(intent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showProgressDialog(){
        pDialog.show();
    }

    @Override
    public void stopProgressDialog() {
        pDialog.dismiss();
    }

    @Override
    public void calculateFreeEstimation(BigInteger gasEstimation) {
        try {
            BigDecimal gasPriceDecimal = new BigDecimal(gasPrice);
            BigDecimal feeEstDecimal = new BigDecimal(gasEstimation).multiply(gasPriceDecimal) ;
            feeEstDecimal = Utility.etherUnitConverter(0, feeEstDecimal, EtherUnit.ether);
            feeEstimation.setText(getString(R.string.feeEstimation) + feeEstDecimal.toPlainString()+" Ether");

            BigDecimal amountDecimal = new BigDecimal(amount_simple.getText().toString());
            amountDecimal = Utility.etherUnitConverter(amountSpinnerPosition, amountDecimal, EtherUnit.ether);
            BigDecimal totalCDecimal = feeEstDecimal.add(amountDecimal);
            totalCost.setText(getString(R.string.total_cost) + String.valueOf(totalCDecimal.toPlainString())+ " Ether");

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void createSpinnerAmountConverter(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity, R.array.etherunit_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerConverter.setAdapter(adapter);
    }

    @Override
    public void checkGasEstimationValue(BigInteger gasEstimation) {
        if(gasEstimation.intValue() > Constants.GAS_ESTIMATION_LIMIT){
            Snackbar.make(rootView, R.string.transaction_will_fail, Snackbar.LENGTH_SHORT).show();
        }else{
            Snackbar.make(rootView, R.string.estimation_complete, Snackbar.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void startTimer() {
//        try {
//            TimerTask task = new TimerTask() {
//                @Override
//                public void run() {
//                    elapsed += INTERVAL;
//                    if (elapsed >= TIMEOUT) {
//                        this.cancel();
//                        updateListTransactions(false);
//
//                    }
//                }
//            };
//
//            Timer timer = new Timer();
//            timer.scheduleAtFixedRate(task, INTERVAL, INTERVAL);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    public void startProgressbar(){
        progress.setVisibility(View.VISIBLE);
        rootView.setAlpha(0.5f);
    }

    @Override
    public void updateListTransactions(boolean success) {
        try {
            adapter = new ListTransactionsAdapter(activity);
            adapter.setSuccess(success);
            adapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //nel caso della simple transaction  effettua l'update della variabile gasPrice
    @Override
    public void updateGasPriceEditText(String gp) {
        gasPrice = gp;
    }

    @Override
    public void updateGasAdvanced(String gas) {}

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void setTo_simple(String qr_value) {
        Log.d(TAG, "setTo_simple: ");
        try {
            EditText to = (EditText) getView().findViewById(R.id.to_simple);
            to.setText(qr_value);
        }catch (Exception e){
            Log.d(TAG, "setTo_simple: ");
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: ");
        try {
            super.onResume();
            if(qr != null) {
                to_simple.setText(qr);
            }
        }catch (Exception e){
            Log.d(TAG, "onResume: ");
            e.printStackTrace();
        }
    }
}