package com.nttdata.etherwallet.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.DetailWalletPresenter;
import com.nttdata.etherwallet.presenter.callback.DetailWalletCallBack;
import com.nttdata.etherwallet.util.Utility;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.view.activity.MainSendTransactionActivity;
import com.nttdata.etherwallet.view.activity.QrCodeActivity;
import com.nttdata.etherwallet.view.activity.SettingsActivity;
import com.nttdata.etherwallet.view.adapter.ListTransactionsAdapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailWalletFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailWalletFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailWalletFragment extends ListFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, DetailWalletCallBack {
    public static String TAG = DetailWalletFragment.class.getSimpleName();

    @BindView(R.id.detail_account_name)
    TextView detail_account_name;

    @BindView(R.id.detail_address)
    TextView detail_address;

    @BindView(R.id.qr_code_button)
    Button qr_code_button;

    @BindView(R.id.detail_balance)
    TextView detail_balance;

    @BindView(R.id.send_button)
    Button send_button;

    @BindView(R.id.convertion_spinner)
    Spinner convertionSpinner;

    private TextView textSpinner;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String accountName;
    private String address;
    private static String balance;
    private static Context mContext;
    private static ListTransactionsAdapter adapter;

    private DetailWalletPresenter mDetailWalletPresenter;

    private static EtherAccount mAccount;
    private Activity activity;

    private boolean check;

    private OnFragmentInteractionListener mListener;

    public DetailWalletFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     //* @param address Parameter 1.
     //* @param name Parameter 2.
     @param account
     * @return A new instance of fragment DetailWalletFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailWalletFragment newInstance(EtherAccount account) {
        Log.d(TAG, "newInstance: ");
        DetailWalletFragment fragment = new DetailWalletFragment();

            try {
                Bundle args = new Bundle();

                mAccount = account;
                if (account.getAddress() != null) {
                    args.putString(ARG_PARAM1, account.getAddress());
                } else {
                    args.putString(ARG_PARAM1, "");
                }
                if (account.getName() != null) {
                    args.putString(ARG_PARAM2, account.getName());
                } else {
                    args.putString(ARG_PARAM2, "");
                }
                if (account.getBalance() != null) {
//                args.putDouble(ARG_PARAM3, account.getBalance());
                    args.putString(ARG_PARAM3, account.getBalance().toString());
                } else {
                    args.putDouble(ARG_PARAM3, 0.0);
                }
                fragment.setArguments(args);

            } catch (Exception e) {
                Log.d(TAG, "newInstance: ");
                e.printStackTrace();
            }


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(false);
        //getActivity().findViewById(R.id.refresh).setVisibility(View.GONE);
        if (getArguments() != null) {
            accountName = getArguments().getString(ARG_PARAM2);
            address = getArguments().getString(ARG_PARAM1);
            balance = getArguments().getString(ARG_PARAM3);
        }
         mDetailWalletPresenter = new DetailWalletPresenter(activity, this);
        //----------------------------------------------
        mDetailWalletPresenter.checkTransactionsValidation(address);
        //----------------------------------------------
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");

        //------------------------------------------------------------------------------
        adapter = new ListTransactionsAdapter(mContext);
        this.setListAdapter(adapter);
        //detail_balance.setText(balance);
        //------------------------------------------------------------------------------

        View view = inflater.inflate(R.layout.fragment_detail_wallet, container, false);
        try{
            ButterKnife.bind(this, view);

            qr_code_button.setOnClickListener(this);
            send_button.setOnClickListener(this);
            convertionSpinner.setOnItemSelectedListener(this);
            mDetailWalletPresenter.balanceRequest(address);

            textSpinner = ((TextView) view.findViewById(R.id.textview_spinner));
            createSpinnerConverter();

            detail_account_name.setText(accountName);
            detail_address.setText(address);
            detail_balance = ((TextView) view.findViewById(R.id.detail_balance));

            BigDecimal balanceDecimal = new BigDecimal(balance);
            EtherUnit to = Utility.getEtherUnitFromPosition(convertionSpinner.getSelectedItemPosition());
            BigDecimal result = Utility.etherUnitConverter(0, balanceDecimal, to);
            detail_balance.setText(String.valueOf(result.toPlainString()));

//            FloatingActionButton floatButton = (FloatingActionButton) activity.findViewById(R.id.fab_main);
//            floatButton.setVisibility(View.GONE);

            activity.setTitle(R.string.wallet_title);

            if(callFromLogistics()) {
                goToSendTransaction();
            }

        }catch (Exception e){
            Log.d(TAG, "onCreateView: ");
            e.printStackTrace();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: ");
        super.onAttach(context);
        mContext = context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            activity = (Activity)context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach: ");
        super.onDetach();
        mListener = null;
    }

    public boolean callFromLogistics(){
        Log.d(TAG, "callFromLogistics: ");
        if(mAccount != null &&  mAccount.getType()!=null && mAccount.getType().equals(Constants.TYPE_LOGISTIC)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick: ");
        if(view.getId() == R.id.qr_code_button){
            if(address != null) {
                Intent intent = new Intent(activity, QrCodeActivity.class);
                intent.putExtra(getString(R.string.intent_address), address);
                startActivity(intent);
            }
        }
        else if(view.getId() == R.id.send_button){
            if(checkInternetConnection()) {
                if (detail_balance.getText() != null && !detail_balance.getText().toString().equals("--") && !detail_balance.getText().toString().equals("")) {
                    goToSendTransaction();
                } else {
                    Toast.makeText(mContext, R.string.noBalance, Toast.LENGTH_LONG).show();
                    showChangeIpDialog();
                }
            }else{
                Toast.makeText(mContext, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
                showChangeIpDialog();
            }
        }
    }

    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public void showChangeIpDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        dialogBuilder.setTitle(R.string.create_new_ip);
        dialogBuilder.setMessage(R.string.change_ip_message);
        dialogBuilder.setPositiveButton(R.string.create_ip_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent(activity, SettingsActivity.class);
                intent.putExtra("changeIP",R.string.changeIP);
                startActivity(intent);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {}
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void createSpinnerConverter(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity, R.array.etherunit_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        convertionSpinner.setAdapter(adapter);
        convertionSpinner.setSelection(6);
    }

    public void goToSendTransaction(){
        Log.d(TAG, "goToSendTransaction: ");
        try {
            Intent intent = new Intent(activity, MainSendTransactionActivity.class);
            HashMap<String, Object> accountMap = new HashMap<>();

            if (callFromLogistics()) {
                if(mAccount.getType() != null) {
                    accountMap.put(Constants.TYPE_PARAM, mAccount.getType());
                }
                if(mAccount.getData() != null) {
                    accountMap.put(Constants.DATA_PARAM, mAccount.getData());
                }
                if(mAccount.getTo() != null) {
                    accountMap.put(Constants.TO_PARAM, mAccount.getTo());
                }
                if(mAccount.getAmount() != null) {
                    accountMap.put(Constants.AMOUNT_PARAM, mAccount.getAmount());
                }
            }

            if(mAccount.getAddress() != null) {
                accountMap.put(Constants.ADDRESS_PARAM, mAccount.getAddress());
            }
            if(mAccount.getBalance() != null) {
                accountMap.put(Constants.BALANCE_PARAM, mAccount.getBalance());
            }
            if(mAccount.getName() != null) {
                accountMap.put(Constants.NAME_PARAM, mAccount.getName());
            }
            if(mAccount.getId() != null) {
                accountMap.put(Constants.ID_PRAM, mAccount.getId());
            }
            if(mAccount.getFile() != null) {
                accountMap.put(Constants.FILE_PARAM, mAccount.getFile());
            }
            if(textSpinner != null && textSpinner.getText() != null) {
                accountMap.put(Constants.ETHER_UNIT, textSpinner.getText().toString());
            }

            intent.putExtra(Constants.ACCOUNT_PARAM, accountMap);
            startActivity(intent);
        }catch (Exception e){
            Log.d(TAG, "goToSendTransaction: ");
            e.printStackTrace();
        }
    }




    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "onItemSelected: "+i);
        try {

            if(check) {
                BigDecimal balanceDecimal = new BigDecimal(balance);
                EtherUnit to = Utility.getEtherUnitFromPosition(i);
                BigDecimal result = Utility.etherUnitConverter(0, balanceDecimal, to);
                detail_balance.setText(String.valueOf(result.toPlainString()));
            }
            check = true;

            //TODO: verificare eliminazione
//            textSpinner = ((TextView) view.findViewById(R.id.textview_spinner));
        }catch (Exception e){
            Log.d(TAG, "onItemSelected: ");
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Log.d(TAG, "onNothingSelected: ");
    }

    @Override
    public void updateBalanceValue(BigInteger balance) {
        BigDecimal balanceDecimal = Utility.etherUnitConverter(0, new BigDecimal(balance), EtherUnit.ether);
        detail_balance.setText(String.valueOf(balanceDecimal.toPlainString()));
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public  void onResume(){
        super.onResume();
        if(mDetailWalletPresenter != null && address != null && address != "") {
            mDetailWalletPresenter.balanceRequest(address);
        }

    }

    //-----------------------------------------------------------------

    public void updateListTransactions(boolean success) {
        try {
            adapter = new ListTransactionsAdapter(activity);
            adapter.setSuccess(success);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                    DetailWalletFragment.this.getListView().invalidateViews();
                    DetailWalletFragment.this.getListView().refreshDrawableState();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mDetailWalletPresenter.stopCheckTransactionsValidation();
    }
    //----------------------------------------------

}