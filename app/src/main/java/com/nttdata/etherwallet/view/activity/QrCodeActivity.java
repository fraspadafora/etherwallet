package com.nttdata.etherwallet.view.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.QrCodePresenter;
import com.nttdata.etherwallet.presenter.callback.QrCodeCallBack;
import com.nttdata.etherwallet.util.http_util.Constants;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QrCodeActivity extends AppCompatActivity implements QrCodeCallBack, View.OnClickListener {

    @BindView(R.id.imageview_qrcode)
    ImageView qr_code_imageview;
    @BindView(R.id.address_text_qrpage)
    TextView address_text;
    @BindView(R.id.back_button)
    Button back_button;
    @BindView(R.id.copyQrCodeAddress)
    ImageButton copy;

    private String address;
    private QrCodePresenter mQrCodePresenter;
    private Bitmap qr_code_bitmap;

    private EtherAccount mAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);

        ButterKnife.bind(this);


        back_button.setOnClickListener(this);
//        if(getIntent().getStringExtra(getString(R.string.intent_address)).equals("qr_reader")) {
//            readQRCode();
//        }else{
//        HashMap<String, Object> map = (HashMap<String, Object>) getIntent().getSerializableExtra("account");
//        mAccount = (EtherAccount) map.get(getString(R.string.intent_address));
//        address = String.valueOf(mAccount.getAddress());
        address = getIntent().getStringExtra(getString(R.string.intent_address));
        mQrCodePresenter = new QrCodePresenter(this, address, this);
        qr_code_bitmap = mQrCodePresenter.generateQrCode();
        qr_code_imageview.setImageBitmap(qr_code_bitmap);
        address_text.setText(address);

        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) QrCodeActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", address);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(QrCodeActivity.this, "Copied!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void readQRCode() {
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        scanIntegrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            Intent intent = getIntent();
            String contents = intent.getStringExtra("SCAN_RESULT"); // This will contain your scan result
            String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.back_button) {
            Intent intent = new Intent(this, MainWalletActivity.class);
//                intent.putExtra(getString(R.string.intent_address), address);
//            HashMap<String, Object> accountMap = new HashMap<>();
//            accountMap.put("account", mAccount);
//            intent.putExtra(Constants.ACCOUNT_PARAM, accountMap);
            startActivity(intent);
        }
    }
}