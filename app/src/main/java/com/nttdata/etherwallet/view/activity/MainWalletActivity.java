package com.nttdata.etherwallet.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.LoadWalletPresenter;
import com.nttdata.etherwallet.presenter.callback.LoadWalletCallBack;
import com.nttdata.etherwallet.presenter.callback.http_callback.VolleyCallBack;
import com.nttdata.etherwallet.presenter.http_presenter.VolleyPresenter;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.view.fragment.DetailWalletFragment;
import com.nttdata.etherwallet.view.fragment.SelectWalletFragment;
import com.nttdata.etherwallet.view.fragment.interaction.OnListFragmentInteractionListener;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainWalletActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, LoadWalletCallBack, VolleyCallBack, OnListFragmentInteractionListener/*, Animation.AnimationListener*/, DetailWalletFragment.OnFragmentInteractionListener {

    private final String TAG = "MainWalletActivity";
    private static final int REQUEST_WALLET = 1;

    @BindView(R.id.rootView_mainWallet)
    FrameLayout rootView;

    @BindView(R.id.progress_bar_main_wallet)
    ProgressBar progressBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_main_wallet)
    FrameLayout contentMainWallet;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private FragmentManager mFragmentManager;
    private LoadWalletPresenter mLoadWalletPresenter;

    private List<EtherAccount> accounts;
    private EtherAccount accSelected;

    private StorageManager mStorageManager;


    @Override
    public void startMain() {
        Log.d(TAG, "startMain ");
        // TODO something general on opening all activities

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_main_wallet);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate, view binding done");

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        mFragmentManager = getSupportFragmentManager();
        mStorageManager = StorageManager.getInstance(this);
        mLoadWalletPresenter = new LoadWalletPresenter(this, this, rootView);

        HashMap<String, Object> mapParameters = (HashMap<String, Object>) getIntent().getSerializableExtra("params_from_logistics");
        if(mapParameters != null){
            try {
                //TODO: i dati provenienti dal marketplace vanno settati una volta presente l'import dell'occount
                String accountImported = (String) mapParameters.get(Constants.ACCOUNT_IMP_PARAM);
                accSelected = mLoadWalletPresenter.getSelectedAccount();
                mStorageManager.setDataAccountFromLogistic(mapParameters, accSelected, Constants.TYPE_LOGISTIC);
                itemSelected(accSelected);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoadWalletPresenter.loadAllWallet();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_wallet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.refresh) {
            getAccountList();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getAccountList(){
        String address = "";
        if (mLoadWalletPresenter.isWalletSelected()) {
            accSelected = mLoadWalletPresenter.getSelectedAccount();
            address = accSelected.getAddress();
        }
        accounts = mLoadWalletPresenter.getAccountsList();

        mFragmentManager.beginTransaction().replace(R.id.content_main_wallet, SelectWalletFragment.newInstance(accounts, address, this), SelectWalletFragment.TAG).commit();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        try {
            int id = item.getItemId();
            if (id == R.id.account_list) {
                getAccountList();
            } else if (id == R.id.import_account) {
                loadWalletFromDisk();
            } else if (id == R.id.settings) {
                goToSettingsActivity();
            }

            drawer.closeDrawer(GravityCompat.START);
        }catch (Exception e){
            Toast.makeText(this, R.string.no_account_found, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return true;
    }

    public void goToSettingsActivity(){
        Log.d(TAG, "goToSettingsActivity: ");
        try {
            accSelected = mLoadWalletPresenter.getSelectedAccount();
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }catch (Exception e){
            Log.d(TAG, "goToSettingsActivity: ");
            e.printStackTrace();
        }
    }

    private void loadWalletFromDisk() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_WALLET);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult");
        if(requestCode == REQUEST_WALLET){
            Log.d(TAG, "onActivityResult REQUEST_WALLET" );
            if(requestCode == RESULT_OK || requestCode == RESULT_FIRST_USER){
                Log.d(TAG, "onActivityResult RESULT_OK" );
                if(data != null && data.getData() != null) {
                    mLoadWalletPresenter.createEditDialog(contentMainWallet, data);
                }else{
                    Snackbar.make(rootView, R.string.no_account_found, Snackbar.LENGTH_SHORT).show();
                }
            }else {
                Log.d(TAG, "onActivityResult RESULT_CANCELLED" );
            }
        }
    }

    @Override
    public void onDoneList(List<EtherAccount> accounts) {
        Log.d(TAG, "onDoneList: " + accounts);
        this.accounts = accounts;
        mFragmentManager.beginTransaction().replace(R.id.content_main_wallet, SelectWalletFragment.newInstance(accounts, null, this), SelectWalletFragment.TAG).commit();
    }

    @Override
    public void onDoneOnce(EtherAccount account) {
        Log.d(TAG, "onDoneOnce " + account);
        if(checkInternetConnection()) {
            if (account.getAddress() != null && account.getName() != null) {
                setAccountAddressNavigationView(account.getAddress(), account.getName());
            }
            mFragmentManager.beginTransaction().replace(R.id.content_main_wallet, DetailWalletFragment.newInstance(account), DetailWalletFragment.TAG).commit();
        }
        else{
            if(mStorageManager.isSelectedEtherAccount()){
                if(mStorageManager.checkForElements(EtherAccount.class)){
                    onDoneList((List<EtherAccount>) mStorageManager.getAll(EtherAccount.class));
                }else{
                    onDoneList(Collections.<EtherAccount>emptyList());
                }
            }
        }
    }

    @Override
    public void onError() {
        Log.d(TAG, "onError");
//        Snackbar.make(fabMain, R.string.generic_error, Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            }
//        });
    }

    @Override
    public void manageProgressBar(boolean state) {
        if(state) {
            progressBar.setVisibility(View.VISIBLE);
            rootView.setAlpha(0.5f);
        }else{
            progressBar.setVisibility(View.GONE);
            rootView.setAlpha(1f);
        }
    }

    @Override
    public void setAccountAddressNavigationView(String address, String name) {
        Log.d(TAG, "setAccountAddressNavigationView: ");
        try {
            navigationView.setNavigationItemSelectedListener(this);
            View hView =  navigationView.getHeaderView(0);
            TextView addressTextView = (TextView) hView.findViewById(R.id.textViewAddress);
            TextView nameTextView = (TextView) hView.findViewById(R.id.navHeaderAccountName);
            nameTextView.setText(name);
            addressTextView.setText(address);
        }catch (Exception e){
            Log.d(TAG, "setAccountAddressNavigationView: ");
            e.printStackTrace();
        }
    }

    @Override
    public void showNoDetailMassage() {
        Snackbar.make(rootView, R.string.noBalance, Snackbar.LENGTH_LONG).show();
    }



    @Override
    public void showNoInternetConnection() {
        Snackbar.make(rootView, R.string.no_internet_connection, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showSelectIPDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        dialogBuilder.setTitle(R.string.change_ip_message);
        dialogBuilder.setMessage(R.string.no_connection);
        dialogBuilder.setPositiveButton(R.string.create_ip_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent(MainWalletActivity.this, SettingsActivity.class);
                intent.putExtra("changeIP",R.string.changeIP);
               startActivity(intent);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {}
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void goToAccountList() {
        getAccountList();
    }

    @Override
    public void onListFragmentInteraction(EtherAccount item) {

    }

    @Override
    public void itemSelected(EtherAccount account) {
        mFragmentManager.beginTransaction().replace(R.id.content_main_wallet, DetailWalletFragment.newInstance(account), DetailWalletFragment.TAG).commit();
    }

    @Override
    public void setAccountSelected(String id) {
        Log.d(TAG, "setAccountSelected: ");
        mLoadWalletPresenter.setSelectedAccount(id);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}