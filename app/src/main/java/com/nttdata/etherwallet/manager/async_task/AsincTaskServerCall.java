package com.nttdata.etherwallet.manager.async_task;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.manager.http_request.Web3JManager;
import com.nttdata.etherwallet.presenter.DetailWalletPresenter;
import com.nttdata.etherwallet.presenter.callback.DetailWalletCallBack;
import com.nttdata.etherwallet.presenter.callback.http_callback.SelectFragmentCallBack;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.util.http_util.UrlServerPath;

import org.web3j.protocol.core.methods.response.EthGetBalance;

import java.math.BigInteger;

/**
 * Created by Francesco on 02/12/2016.
 */

public class AsincTaskServerCall extends AsyncTask<Void, Void, BigInteger>{

    private static final String TAG = AsincTaskServerCall.class.getSimpleName();

    private Web3JManager mWeb3JManager;
    private String address;
    private BigInteger balance;
    private StorageManager mStorageManager;
    private DetailWalletCallBack mCallBack;
    SelectFragmentCallBack mSelectFragmentCallBack;

    public AsincTaskServerCall(/*Web3JManager web3JManager, */String address, BigInteger balance, Context context/*StorageManager storageManager*/, DetailWalletCallBack callBack, SelectFragmentCallBack selectCallBack){
//        this.mWeb3JManager = web3JManager;
        //mWeb3JManager = Web3JManager.getInstance(/*"http://168.128.12.89:8545/"*/UrlServerPath.ip_ntt);
        if(Constants.IP_SELECTED!= null && Constants.IP_SELECTED != "") {
            mWeb3JManager = Web3JManager.getInstance(Constants.IP_SELECTED);
        }else{
            mWeb3JManager = Web3JManager.getInstance(Constants.IP_DEFAULT);
        }
        this.address = address;
        this.balance = balance;
        mStorageManager = StorageManager.getInstance(context);
//        this.mStorageManager = storageManager;
        if (callBack != null) {
            this.mCallBack = callBack;
        }else{
            this.mSelectFragmentCallBack = selectCallBack;
        }


    }


    @Override
    protected BigInteger doInBackground(Void... voids) {
        EthGetBalance ethBalance = null;
        try {
            ethBalance = mWeb3JManager.getBalance(address);
            balance = ethBalance.getBalance();

//            mStorageManager.updateBalance(address, balance);
//            if(mCallBack != null){
//                mCallBack.updateBalanceValue(balance);
//            }else{
//                mSelectFragmentCallBack.updateAccountList();
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return balance;
    }

    @Override
    protected void onPostExecute(BigInteger b){
        Log.d(TAG, "onPostExecute: ");
        try {
            if(b != null) {
                mStorageManager.updateBalance(address, b);
               /* if (mCallBack != null) {
                    mCallBack.updateBalanceValue(b);
                } else {
                    mSelectFragmentCallBack.updateAccountList();
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if(b!=null) {
                if (mCallBack != null) {
                    mCallBack.updateBalanceValue(b);
                } else {
                    mSelectFragmentCallBack.updateAccountList();
                }
            }
        }
    }
}