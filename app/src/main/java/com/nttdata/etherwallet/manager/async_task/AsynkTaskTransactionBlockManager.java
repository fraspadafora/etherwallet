package com.nttdata.etherwallet.manager.async_task;

import android.os.AsyncTask;
import android.util.Log;

import com.nttdata.etherwallet.manager.http_request.Web3JManager;
import com.nttdata.etherwallet.model.transaction.Transaction;
import com.nttdata.etherwallet.presenter.http_presenter.SendTransactionPresenter;
import com.nttdata.etherwallet.util.http_util.Constants;
import com.nttdata.etherwallet.util.http_util.UrlServerPath;

/*import org.bouncycastle.crypto.ec.ECElGamalDecryptor;
import org.web3j.abi.datatypes.Address;*/
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Francesco on 22/12/2016.
 */

public class AsynkTaskTransactionBlockManager  extends AsyncTask<HashMap<String, Object>, Void, EthGetTransactionReceipt> {
    public static final String TAG = AsynkTaskTransactionBlockManager.class.getSimpleName();

    String txId, address;
    List<Transaction> list;
    HashMap<String, Object> mapParameters;
    Transaction tr;

    private static final long WAITING_TIME = 5000;

    EthGetTransactionReceipt blockResult;
    Web3JManager mWeb3JManager;
    private SendTransactionPresenter mSendTransactionPresenter;

    public AsynkTaskTransactionBlockManager(SendTransactionPresenter sender) {
        if(Constants.IP_SELECTED!= null && Constants.IP_SELECTED != "") {
            mWeb3JManager = Web3JManager.getInstance(Constants.IP_SELECTED);
        }else{
            mWeb3JManager = Web3JManager.getInstance(Constants.IP_DEFAULT);
        }
        //mWeb3JManager = Web3JManager.getInstance(/*"http://168.128.12.89:8545/"*/UrlServerPath.ip_ntt);
        mSendTransactionPresenter = sender;
    }

    @Override
    protected EthGetTransactionReceipt doInBackground(HashMap<String, Object>... inputParams) {
        Log.d(TAG, "doInBackground: ");

        try {
            if (inputParams[0] != null) {

                mapParameters = inputParams[0];

                if(mapParameters.get(Constants.ADDRESS_PARAM) != null) {
                    address = mapParameters.get(Constants.ADDRESS_PARAM).toString();
                }

                if(mapParameters.get(Constants.LIST_TRANSACTION_PARAM) != null) {
                    list = (List<Transaction>) mapParameters.get(Constants.LIST_TRANSACTION_PARAM);
                }

                if(mapParameters.get(Constants.TXID_PARAM) != null) {
                    txId = mapParameters.get(Constants.TXID_PARAM).toString();
                }

                if(mapParameters.get(Constants.TRANSACTION_PARAM) != null){
                    tr = (Transaction) mapParameters.get(Constants.TRANSACTION_PARAM);
                }

                blockResult =  mWeb3JManager.runBlockInTransaction(txId);
//                checkTransactionInBlock(blockResult);

            }
        }catch (Exception e){
            Log.d(TAG, "doInBackground: EXCEPTION: "+e);
            e.printStackTrace();
        }

        return blockResult;
    }

    @Override
    protected void onPostExecute(EthGetTransactionReceipt response) {
        Log.d(TAG, "onPostExecute: START");
        super.onPostExecute(response);
        try {
            mapParameters.put(Constants.BLOCK_RESULT_PARAM, response);
            mapParameters.put(Constants.TRANSACTION_PARAM, tr);
//            mapParameters.put(Constants.LIST_TRANSACTION_PARAM, list);
            Thread.sleep(WAITING_TIME);
            mSendTransactionPresenter.sendTransactionInBlock(mapParameters);
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "onPostExecute: ");
        }
    }
    
//    public void checkTransactionInBlock(EthGetTransactionReceipt response){
//        Log.d(TAG, "checkTransactionInBlock: ");
//        try {
//            if (blockResult.getTransactionReceipt() != null) {
//                mapParameters.put(Constants.BLOCK_RESULT_PARAM, response);
//            } else {
//                checkTransactionInBlock(response);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//            Log.d(TAG, "checkTransactionInBlock: EXCEPTION");
//        }
//    }
}
