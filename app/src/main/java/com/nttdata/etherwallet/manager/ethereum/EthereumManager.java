package com.nttdata.etherwallet.manager.ethereum;

import android.content.Context;
import android.util.Log;

import com.nttdata.ethereum.keys.decryptor.EthMistAccountDecryptor;
import com.nttdata.ethereum.keys.decryptor.PrivateKeyDecriptor;
import com.nttdata.ethereum.utils.Util;
import org.ethereum.core.Transaction;

import java.math.BigInteger;

/**
 * Created by Francesco on 22/11/2016.
 */

public class EthereumManager {

    private static final String TAG = EthereumManager.class.getSimpleName();

    private static EthereumManager ethInstance;
    //private AccountManager am;

    private EthereumManager(Context mContext) {
        //this.am = Geth.newAccountManager(String.valueOf(mContext.getFilesDir()), Geth.LightScryptN, Geth.LightScryptP);
    }

    public static EthereumManager getInstance(Context context) {
        if (ethInstance == null) {
            ethInstance = new EthereumManager(context);
        }
        return ethInstance;
    }

//    public Account importAccount(String file, String exportPass, String importPass){
//        Account account = null;
//        try {
//            account = am.importKey(file.getBytes(), exportPass, importPass);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return account;
//    }
//
//    public void unlock(Account account, String password){
//        try {
//            am.unlock(account, password);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public String createSignedTransaction(String to, BigInteger amount, BigInteger nonce, BigInteger gasPrice, BigInteger gasLimit, Account account){
//        String signedTransaction = null;
//        try {
//            Transaction tx = Transaction.create(Util.stripHexPrefix(to), amount, nonce, gasPrice, gasLimit);
//            byte[] signature = am.sign(account.getAddress(), tx.getRawHash());
//
//            byte[] signedTx = tx.getEncoded(signature);
//            signedTransaction = Util.encodeHexString(signedTx);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return signedTransaction;
//    }

    public byte[] generatePrivateKey(String file, String password){
        byte[] privateKey = null;
        try {
            PrivateKeyDecriptor decriptor = new EthMistAccountDecryptor();
            privateKey = decriptor.decrypt(file, password);
        }catch (Exception e){
            e.printStackTrace();
        }
        return privateKey;
    }


    public String createSignedTransactionWithPrivateKey(byte[] privateKey, String to, BigInteger amount, BigInteger nonce, BigInteger gasPrice, BigInteger gasLimit, byte[] data){
        Log.d(TAG, "createSignedTransactionWithPrivateKey start ");
        String signedTransaction = null;
        try {

            Transaction tx = Transaction.create(Util.stripHexPrefix(to), amount, nonce, gasPrice, gasLimit, data);
            tx.sign(privateKey);
            byte[] signedTx = tx.getEncoded();
            signedTransaction = Util.encodeHexString(signedTx);

        }catch (Exception e){
            Log.d(TAG, "createSignedTransactionWithPrivateKey Exception ");
            e.printStackTrace();
        }
        Log.d(TAG, "createSignedTransactionWithPrivateKey end ");
        return signedTransaction;
    }

}