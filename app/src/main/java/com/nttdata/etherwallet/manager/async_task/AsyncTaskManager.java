package com.nttdata.etherwallet.manager.async_task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.presenter.http_presenter.SendTransactionPresenter;
import com.nttdata.etherwallet.util.Utility;
import com.nttdata.etherwallet.util.http_util.Constants;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by Francesco on 02/12/2016.
 */

public class AsyncTaskManager extends AsyncTask<HashMap<String, Object>, Void, String> {
    private static final String TAG = AsyncTaskManager.class.getSimpleName();

    private SendTransactionPresenter mSendTransactionPresenter;
    private String to;
    private String amount;
    private double gasPrice;
    private int gasEstimation;
    private String data = null;
    private EtherAccount mAccount;

    private ProgressDialog pDialog;
    private View rootView;

    public AsyncTaskManager(View view, ProgressDialog progress, SendTransactionPresenter sender){
        mSendTransactionPresenter = sender;
        pDialog = progress;
        rootView = view;
    }

    @Override
    protected String doInBackground(HashMap<String, Object>... map) {
        Log.d(TAG, "doInBackground: ");
        String signedTransaction = null;
        try {
            HashMap<String, Object> mapParameters = map[0];
            if (map != null) {

                String password = mapParameters.get(Constants.PASSWORD_PARAM).toString();
                to = mapParameters.get(Constants.TO_PARAM).toString();
                int nonce = Integer.parseInt(mapParameters.get(Constants.NONCE_PARAM).toString());

                amount = mapParameters.get(Constants.AMOUNT_PARAM).toString();
                int positionAmomunt = (Integer) mapParameters.get(Constants.ETHER_UNIT_AMOUNT);
                BigDecimal amountDecimal = new BigDecimal(amount);
                amountDecimal = Utility.etherUnitConverter(positionAmomunt, amountDecimal, EtherUnit.wei);
                amount = String.valueOf(amountDecimal);

                int positionGasPrice = 0;
                if(mapParameters.get(Constants.ETHER_UNIT_GAS_PRICE) != null && ((Integer)mapParameters.get(Constants.ETHER_UNIT_GAS_PRICE))!= -1){
                    positionGasPrice = (Integer) mapParameters.get(Constants.ETHER_UNIT_GAS_PRICE);
                }
                BigDecimal gasPriceDecimal = new BigDecimal(mapParameters.get(Constants.GAS_PRICE_PARAM).toString());
                gasPriceDecimal = Utility.etherUnitConverter(positionGasPrice, gasPriceDecimal, EtherUnit.wei);
                gasPrice = Double.parseDouble(String.valueOf(gasPriceDecimal));

                gasEstimation = Integer.parseInt(mapParameters.get(Constants.GAS_ESTIMATION_PARAM).toString());
                mAccount = (EtherAccount) mapParameters.get(Constants.ACCOUNT_PARAM);
                if(mapParameters.get(Constants.DATA_PARAM)!= null) {
                    data = mapParameters.get(Constants.DATA_PARAM).toString();
                }
                signedTransaction = mSendTransactionPresenter.generateSignedTransaction(password, to, nonce, amountDecimal, gasPrice, gasEstimation, mAccount, data);
            }
        }catch (Exception e){
            Log.d(TAG, "doInBackground: ");
//            pDialog.dismiss();
            e.printStackTrace();
        }

        return signedTransaction;
    }

    @Override
    protected void onPostExecute(String signedTransaction) {
        Log.d(TAG, "onPostExecute: ");
        try {
            if(signedTransaction != null ) {
                HashMap<String, Object> mapInputPrameters = new HashMap<>();
                mapInputPrameters.put(Constants.TO_PARAM, to);
                mapInputPrameters.put(Constants.AMOUNT_PARAM, amount);
                mapInputPrameters.put(Constants.SIGNED_TRANSACTION_PARAM, signedTransaction);
                mapInputPrameters.put(Constants.TRANSACTIONS_LIST, mAccount.getTransactionList());
                mapInputPrameters.put(Constants.ADDRESS_PARAM, mAccount.getAddress());
                mapInputPrameters.put(Constants.GAS_PARAM, gasEstimation);
                mapInputPrameters.put(Constants.GAS_PRICE_PARAM, gasPrice);

                mSendTransactionPresenter.sendRowInTransaction(mapInputPrameters, mAccount);
            }else{
                pDialog.dismiss();
                Snackbar.make(rootView, R.string.error_transaction, Snackbar.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Log.d(TAG, "onPostExecute: ");
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

}
