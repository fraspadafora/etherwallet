package com.nttdata.etherwallet.manager.http_request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Francesco on 16/11/2016.
 */

public class RequestManager extends Request<String> {
    public static final String TAG = RequestManager.class.getSimpleName();

    private Response.Listener<String> listener;
    private int TIMEOUT_MS = 30000;
    private final HashMap headersMap;

    public RequestManager(int method, String url, String params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);

        this.headersMap = new HashMap();
        this.headersMap.put("source", "ANDROID");
        this.listener = listener;
        setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Log.d(TAG, "parseNetworkResponse " + response.statusCode);
        try {
            String parsedResponse = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new String(parsedResponse), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (Exception je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(String response) {
        listener.onResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headersMap;
    }
}
