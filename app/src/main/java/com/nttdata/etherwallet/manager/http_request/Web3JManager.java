package com.nttdata.etherwallet.manager.http_request;

import android.app.Application;
import android.util.Log;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthEstimateGas;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

/**
 * Created by Francesco on 20/12/2016.
 */

public class Web3JManager extends Application{
    private final String TAG = Web3JManager.class.getSimpleName();

    private static Web3JManager webInstance;
    private Web3j web3j;

    private Web3JManager(String url){
        web3j = Web3jFactory.build(new HttpService(url));
    }

    public static Web3JManager getInstance(String url) {
        if (webInstance == null) {
            webInstance = new Web3JManager(url);
        }
        return webInstance;
    }

    public EthGetTransactionCount getNonce(String address)throws Exception{
        Log.d(TAG, "getNonce: start");
        EthGetTransactionCount txCount = web3j.ethGetTransactionCount(address, DefaultBlockParameterName.LATEST).sendAsync().get();
        Log.d(TAG, "getNonce: end: "+txCount);
        return txCount;
    }
    
    public EthGetBalance getBalance(String address)throws Exception{
        Log.d(TAG, "getBalance: start");
        EthGetBalance balance = web3j.ethGetBalance(address, DefaultBlockParameterName.LATEST).sendAsync().get();
        Log.d(TAG, "getBalance: end");
        return balance;
    }

    public EthGasPrice getGasPrice()throws Exception{
        Log.d(TAG, "getGasPrice: ");
        EthGasPrice gasPrice = web3j.ethGasPrice().sendAsync().get();
        Log.d(TAG, "getGasPrice: end: "+gasPrice);
        return gasPrice;
    }

    public EthEstimateGas getEstimateGas(String from, String to, BigInteger amount, BigInteger nonce, String data)throws Exception{
        Log.d(TAG, "getGasPrice: ");
        Transaction tx = Transaction.createFunctionCallTransaction(from, nonce, null, null, to, amount, data);
        EthEstimateGas estimateGas = web3j.ethEstimateGas(tx).sendAsync().get();
        Log.d(TAG, "getGasPrice: end: "+estimateGas);
        return estimateGas;
    }

    public EthSendTransaction runRawTransaction(String signedTransactionData)throws  Exception{
        Log.d(TAG, "runRowTransaction: START");
        EthSendTransaction transaction = web3j.ethSendRawTransaction(signedTransactionData).sendAsync().get();
        Log.d(TAG, "runRowTransaction: END "+transaction);
        return  transaction;
    }

    public EthGetTransactionReceipt runBlockInTransaction(String txId)throws Exception{
        Log.d(TAG, "runBlockInTransaction: START");
        EthGetTransactionReceipt blockTransaction = web3j.ethGetTransactionReceipt(txId).sendAsync().get();
        Log.d(TAG, "runBlockInTransaction: END");
        return blockTransaction;
    }

}
