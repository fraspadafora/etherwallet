package com.nttdata.etherwallet.manager.http_request;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nttdata.etherwallet.model.http_response.BaseResponse;
import com.nttdata.etherwallet.util.http_util.ResultCode;

import org.json.JSONObject;

import java.lang.reflect.Type;

/**
 * Created by Francesco on 16/11/2016.
 */

public class VolleyManager extends Application{

    public interface ResponseHandler {
        void success(ResultCode code, String response);
        void failure(ResultCode code, String response, Exception error);
    }

    public static final String TAG = VolleyManager.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private static VolleyManager vmInstance;

    private VolleyManager(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static VolleyManager getInstance(Context context) {
        if (vmInstance == null) {
            vmInstance = new VolleyManager(context);
        }
        return vmInstance;
    }

    public void generateRequest(int type, String url, JSONObject request,  final ResponseHandler handler){
        Log.d(TAG, "generateRequest: ");
        try {

            JsonObjectRequest postRequest = new JsonObjectRequest(type, url,

                    request,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, "onResponse: " + response);

                            Gson gson = new Gson();
                            Type typeToken = new TypeToken<BaseResponse>() {
                            }.getType();
                            BaseResponse mBaseResponse = gson.fromJson(String.valueOf(response), typeToken);

                            ResultCode resultCode = ResultCode.valueOf(mBaseResponse.getStatus());
                            if (handler != null) {
                                switch (resultCode) {
                                    case  RESPONSE_SUCCESSFUL:
                                        handler.success(resultCode, response.toString());
                                        break;
                                    default:
                                        handler.failure(resultCode, String.valueOf(response), new Exception(response.toString()));
                                        break;
                                }
                            } else {
                                Log.d(TAG, "no external handler");
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, "onErrorResponse: " + error);

                            if (handler != null) {
                                ResultCode code = ResultCode.GENERIC_ERROR;
                                handler.failure(code, null, error);
                            } else {
                                Log.d(TAG, "no external handler");
                            }
                        }
                    }) {
            };
            mRequestQueue.add(postRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}