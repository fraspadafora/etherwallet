package com.nttdata.etherwallet.manager.data_base;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RadioButton;

import com.nttdata.etherwallet.model.EtherAccount;
import org.apache.commons.lang3.StringUtils;
import com.nttdata.etherwallet.model.ip_model.IP;
import com.nttdata.etherwallet.model.transaction.Transaction;
import com.nttdata.etherwallet.presenter.http_presenter.SendTransactionPresenter;
import com.nttdata.etherwallet.util.http_util.Constants;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by FarandaAl on 08/11/2016.
 */
public class StorageManager {

    private final String TAG = StorageManager.class.getSimpleName();



    interface SharedKey{
        String CURRENT_WALLET_ID = "current_wallet_id";
    }

    private static StorageManager ourInstance;
    private String currentWalletId;
    private final Realm realm;
    private final SharedPreferences shared;

    private StorageManager(Context context) {
        Realm.init(context);
        realm = Realm.getDefaultInstance();
        shared = context.getSharedPreferences("StorageManager", Context.MODE_PRIVATE);
        onInit();
    }

    private void onInit() {
        currentWalletId = shared.getString(SharedKey.CURRENT_WALLET_ID, "");
    }

    public void saveObject(RealmObject realmObject) {
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(realmObject);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (realm.isInTransaction()) {
                    realm.cancelTransaction();
                }
            } catch (Exception e1) {
                e.printStackTrace();
            }
        }
    }

    public IP setIpSelected(IP ip, boolean ipSelected) {
        realm.beginTransaction();
        ip.setSelected(ipSelected);
        realm.copyToRealmOrUpdate(ip);
        realm.commitTransaction();
        return ip;
    }

    public void setIpRadioChecked(IP ip, int  position) {
        realm.beginTransaction();
        ip.setRadioChecked(position);
        realm.commitTransaction();
    }

    public void updateTransactionInBlock(Transaction tr, RealmList<Transaction> list, boolean state, String block, String address){
        Log.d(TAG, "updateTransactionInBlock: ");
        try {
            realm.beginTransaction();
            tr.setValidate(state);
            tr.setBlockNumber(block);
            realm.copyToRealm(tr);
            list.remove(tr);
            list.add(tr);
            EtherAccount account = realm.where(EtherAccount.class).equalTo("address", address).findFirst();
            account.setTransactionList(list);
            realm.copyToRealmOrUpdate(account);
            realm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public RealmList<IP> addDefaultIP(RealmList<IP> ipList, IP default_ip) {
        try {
            realm.beginTransaction();
            if(ipList.size() == 0){
                ipList.add(default_ip);
            }
            realm.commitTransaction();
            return ipList;
        }catch(Exception e){
            Log.d(TAG, "addDefaultIP Exception: "+e);
            e.printStackTrace();
            return null;
        }
    }

    public void updateBalance(String address, BigInteger balance){

        Log.d(TAG, "updateBalance: ");
        realm.beginTransaction();
        EtherAccount account = realm.where(EtherAccount.class).equalTo("address", address).findFirst();
        account.setBalance(balance.toString());
        realm.copyToRealmOrUpdate(account);
        realm.commitTransaction();
    }

    public EtherAccount setDataAccountFromLogistic(HashMap<String, Object> mapData, EtherAccount account, String type){
        Log.d(TAG, "setDataAccountFromLogistic: ");
            try {
                String to = null;
                String data = null;
                if(mapData != null){
                    data = mapData.get(Constants.DATA_PARAM).toString();
                    to = mapData.get(Constants.TO_PARAM).toString();
                }else{
                    to = account.getTo();
                    data = account.getData();
                }
                realm.beginTransaction();
                String amount = "0";
                account.setData(data);
                account.setTo(to);
                account.setAmount(amount);
                account.setType(type);
                realm.copyToRealmOrUpdate(account);
                realm.commitTransaction();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
            return account;
    }

    public List<? extends RealmObject> getAll(Class<? extends RealmObject> clz){
        Log.d(TAG, "getAll: ");
        return realm.where(clz).findAll();
    }

    public static StorageManager getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new StorageManager(context);
        }
        return ourInstance;
    }

    public EtherAccount getSelectedEtherAccount() {
        Log.d(TAG, "getSelectedEtherAccount: ");
        return realm.where(EtherAccount.class).equalTo("id", currentWalletId).findFirst();
    }

    public RealmList<Transaction> getTransactionsList(String address){
        Log.d(TAG, "getTransactionsList: ");
        return realm.where(EtherAccount.class).equalTo("address", address).findFirst().getTransactionList();
    }

    public RealmList<IP> getIPList(String address){
        Log.d(TAG, "getIPList: ");
        return realm.where(EtherAccount.class).equalTo("address", address).findFirst().getIpList();
    }

    public boolean checkForElements(Class<? extends RealmObject> classType) {
        Log.d(TAG, "checkForElements: ");
        return realm.where(classType).findAll().size() > 0;
    }

    public void setSelectedEtherAccount(String currentWalletId) {
        Log.d(TAG, "setSelectedEtherAccount: ");
        shared.edit().putString(SharedKey.CURRENT_WALLET_ID, currentWalletId).commit();
        this.currentWalletId = currentWalletId;
    }

   public boolean isSelectedEtherAccount(){
       Log.d(TAG, "isSelectedEtherAccount: ");
       return StringUtils.isNotEmpty(shared.getString(SharedKey.CURRENT_WALLET_ID, "")) ;
   }

    public RealmList<Transaction> createAndGetListTransaction(final HashMap<String, Object> mapParameters, SendTransactionPresenter mSenderTransactionPresenter){
        Log.d(TAG, "manageTransactionsBlock: START");
        RealmList<Transaction> list = new RealmList<>();

        try {
            realm.beginTransaction();
            String address = (String) mapParameters.get(Constants.ADDRESS_PARAM);
            Transaction tr = realm.copyToRealmOrUpdate(mSenderTransactionPresenter.createTransaction(mapParameters));
            list = getTransactionsList(address);
            list.add(tr);
            realm.commitTransaction();

        }catch (Exception e){
            e.printStackTrace();
        }

        return list;
    }

    public void addNewIP(EtherAccount accSelected, String ip, String port) {
        Log.d(TAG, "addNewIP: ");
        try {
            realm.beginTransaction();
            //IP ipObject = realm.copyToRealmOrUpdate(new IP(ip, port));
            IP ipObject = realm.copyToRealm(new IP(ip, port));
            RealmList<IP> list = accSelected.getIpList();
            list.add(ipObject);
//            realm.copyToRealmOrUpdate(accSelected);
            realm.copyToRealm(accSelected);
            realm.commitTransaction();
        }catch (Exception e){
            Log.d(TAG, "addNewIP: EXCEPTION: "+e);
            e.printStackTrace();
            realm.commitTransaction();
        }
    }
}