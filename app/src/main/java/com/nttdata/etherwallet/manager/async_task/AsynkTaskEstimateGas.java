package com.nttdata.etherwallet.manager.async_task;

import android.os.AsyncTask;
import android.util.Log;

import com.nttdata.etherwallet.manager.http_request.Web3JManager;
import com.nttdata.etherwallet.presenter.callback.http_callback.SendTransactionCallBack;

import org.web3j.protocol.core.methods.response.EthEstimateGas;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;

import java.math.BigInteger;

/**
 * Created by SpadaforaFr on 04/03/2017.
 */

public class AsynkTaskEstimateGas<Void> extends AsyncTask {

    private String from;
    private String to;
    private String amt;
    private String data;
    private SendTransactionCallBack mCallBack;
    private Web3JManager mWeb3jManager;
    private BigInteger gasEstimation;
    private BigInteger gasPrice;

    public AsynkTaskEstimateGas(String from, String to, String amt, String data, SendTransactionCallBack mCallBack, Web3JManager mWeb3jManager) {
        this.from = from;
        this.to = to;
        this.amt = amt;
        this.data = data;
        this.mCallBack = mCallBack;
        this.mWeb3jManager = mWeb3jManager;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            BigInteger amount = new BigInteger(amt);
            EthGetTransactionCount ethgetTransaction = mWeb3jManager.getNonce(from);
            BigInteger nonce = ethgetTransaction.getTransactionCount();
            EthEstimateGas eGas = mWeb3jManager.getEstimateGas(from, to, amount, nonce, data);
            EthGasPrice gPrice = mWeb3jManager.getGasPrice();
            gasPrice = gPrice.getGasPrice();
            gasEstimation = eGas.getAmountUsed();

        }catch(Exception e){
            Log.e("AsynkTaskEstimateGas", "Exception", e);
            return e;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        if(o==null){
            mCallBack.updateGasAdvanced(String.valueOf(gasEstimation));
            mCallBack.updateGasPriceEditText(gasPrice.toString());
            mCallBack.calculateFreeEstimation(gasEstimation);
            mCallBack.checkGasEstimationValue(gasEstimation);
        }
        else{
            mCallBack.showSnackBar("Error Estimating Gas, plesae try again.");
        }
    }


}
