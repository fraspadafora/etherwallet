package com.nttdata.etherwallet.model.http_request;

/**
 * Created by Francesco on 17/11/2016.
 */

public class BlockChainTransactionRequest {

    public String txId;

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}
