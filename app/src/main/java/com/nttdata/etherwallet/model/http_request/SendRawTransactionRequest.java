package com.nttdata.etherwallet.model.http_request;

/**
 * Created by Francesco on 17/11/2016.
 */

public class SendRawTransactionRequest {

    public String signedTransaction;

    public String getSignedTransaction() {
        return signedTransaction;
    }

    public void setSignedTransaction(String signedTransaction) {
        this.signedTransaction = signedTransaction;
    }
}
