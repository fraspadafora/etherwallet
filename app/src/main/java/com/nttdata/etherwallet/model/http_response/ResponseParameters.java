package com.nttdata.etherwallet.model.http_response;

import com.google.android.gms.drive.query.internal.MatchAllFilter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;


/**
 * Created by Francesco on 16/11/2016.
 */

public class ResponseParameters{

    @SerializedName("balance")
    @Expose
    private double balance;

    @SerializedName("nonce")
    @Expose
    private int nonce;

    @SerializedName("gasEstimation")
    @Expose
    private int gasEstimation;

    @SerializedName("gasPrice")
    @Expose
    private double gasPrice;

    @SerializedName("txId")
    @Expose
    private String txId;

    @SerializedName("blockNumber")
    @Expose
    private BigInteger blockNumber;

    @SerializedName("blockHash")
    @Expose
    private String blockHash;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
//        this.balance = Math.pow(10,balance);
        this.balance = Math.round(balance);
    }

    public int getNonce() {
        return nonce;
    }

    public void setNonce(int nonce) {
        this.nonce = nonce;
    }

    public int getGasEstimation() {
        return gasEstimation;
    }

    public void setGasEstimation(int gasEstimation) {
        this.gasEstimation = gasEstimation;
    }

    public double getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(double gasPrice) {
        this.gasPrice = Math.pow((double)10,(double)gasPrice);
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public BigInteger getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(BigInteger blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }
}