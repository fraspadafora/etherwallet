package com.nttdata.etherwallet.model.http_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 17/11/2016.
 */

public class NonceResponse extends BaseResponse{

    @SerializedName("nonce")
    @Expose
    public Integer nonce;

    public Integer getNonce() {
        return nonce;
    }

    public void setNonce(Integer nonce) {
        this.nonce = nonce;
    }
}
