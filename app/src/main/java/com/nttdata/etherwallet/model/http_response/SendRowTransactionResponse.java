package com.nttdata.etherwallet.model.http_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 16/11/2016.
 */

public class SendRowTransactionResponse extends BaseResponse{

    @SerializedName("txId")
    @Expose
    public String txId;


    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}