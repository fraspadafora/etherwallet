
package com.nttdata.etherwallet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Cipherparams extends RealmObject {

    @SerializedName("iv")
    @Expose
    private String iv;

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

}
