package com.nttdata.etherwallet.model.http_response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 17/11/2016.
 */

public class DataResponse extends BaseResponse{

    @SerializedName("data")
    public ResponseParameters data;

    public ResponseParameters getData() {
        return data;
    }

    public void setData(ResponseParameters data) {
        this.data = data;
    }
}
