
package com.nttdata.etherwallet.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nttdata.etherwallet.model.ip_model.IP;
import com.nttdata.etherwallet.model.transaction.Transaction;
import com.nttdata.etherwallet.util.http_util.Constants;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

//TODO da rifattorizzare
public class EtherAccount extends RealmObject {

    private String type;

    private String name;

    private String file;

//    private Double balance;

    private String balance;

    private String amount;

    //da rifattorizzare
    private String data;

    private String to;

    private Transaction transaction;

    private RealmList<Transaction> transactionList;

    private RealmList<IP> ipList;


    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("Crypto")
    @Expose
    private Crypto crypto;

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("version")
    @Expose
    private Integer version;

    public RealmList<IP> getIpList() {
        return ipList;
    }

    public void setIpList(RealmList<IP> ipList) {
        this.ipList = ipList;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Crypto getCrypto() {
        return crypto;
    }

    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public RealmList<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(RealmList<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}