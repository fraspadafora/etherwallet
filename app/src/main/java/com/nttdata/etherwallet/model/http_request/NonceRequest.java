package com.nttdata.etherwallet.model.http_request;

/**
 * Created by Francesco on 17/11/2016.
 */

public class NonceRequest {

    public String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
