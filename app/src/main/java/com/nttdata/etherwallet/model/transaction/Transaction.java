package com.nttdata.etherwallet.model.transaction;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Francesco on 22/11/2016.
 */

public class  Transaction extends RealmObject implements Serializable{

    private int id;

    private String address;
    private String to;
    private String amount;

    @PrimaryKey
    private String txId;
    private Date timeStamp;
    private String blockNumber;
    private boolean validate;

    private String gas;
    private String gasPrice;
    private String data;

    private boolean timer;

    public Transaction(){

    }

    public Transaction(String address, String to, String amount, String txId, boolean validate, Date timeStamp, String blockNumber, String gas, String gasPrice) {
        this.to = to;
        this.address = address;
        this.amount = amount;
        this.txId = txId;
        this.timeStamp = timeStamp;
        this.blockNumber = blockNumber;
        this.validate = validate;
        this.gas = gas;
        this.gasPrice = gasPrice;
    }

    public Transaction(String address, String to, String amount, String data) {
        this.to = to;
        this.address = address;
        this.amount = amount;
        this.data = data;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public boolean isValidate() {

        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }

    public String getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(String gasPrice) {
        this.gasPrice = gasPrice;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTimer() {
        return timer;
    }

    public void setTimer(boolean timer) {
        this.timer = timer;
    }
}