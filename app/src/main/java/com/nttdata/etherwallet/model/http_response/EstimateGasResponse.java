package com.nttdata.etherwallet.model.http_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 16/11/2016.
 */

public class EstimateGasResponse extends BaseResponse{

    @SerializedName("gasEstimation")
    @Expose
    public Integer gasEstimation;


    public Integer getGasEstimation() {
        return gasEstimation;
    }

    public void setGasEstimation(Integer gasEstimation) {
        this.gasEstimation = gasEstimation;
    }
}
