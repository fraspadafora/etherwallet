package com.nttdata.etherwallet.model;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by Francesco on 21/11/2016.
 */

public class EtherTransaction extends RealmObject {

    private String to;
    private Double amount;
    private String txId;
    private Date timeStamp;
    private Double blockNumber;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Double getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(Double blockNumber) {
        this.blockNumber = blockNumber;
    }
}