package com.nttdata.etherwallet.model.ip_model;

import android.widget.RadioButton;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Francesco on 19/12/2016.
 */

public class IP extends RealmObject{

    @PrimaryKey
    private String ip_address;
    private String ip_port;
    private boolean selected;
    private int radioChecked;

    public IP(String ip_address, String ip_port) {
        this.ip_address = ip_address;
        this.ip_port = ip_port;
        selected = false;
        radioChecked = -1;
    }

    public IP( ) {}


    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getIp_port() {
        return ip_port;
    }

    public void setIp_port(String ip_port) {
        this.ip_port = ip_port;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setRadioChecked(int radioChecked) {
        this.radioChecked = radioChecked;
    }

    public int getRadioChecked() {
        return radioChecked;
    }
}
