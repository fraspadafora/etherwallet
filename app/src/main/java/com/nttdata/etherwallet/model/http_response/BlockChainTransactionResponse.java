package com.nttdata.etherwallet.model.http_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 17/11/2016.
 */

public class BlockChainTransactionResponse extends BaseResponse{

    @SerializedName("blockNumber")
    @Expose
    public Integer blockNumber;

    @SerializedName("blockHash")
    @Expose
    public String blockHash;

    public Integer getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(Integer blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }
}
