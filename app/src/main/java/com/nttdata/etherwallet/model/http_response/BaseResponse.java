package com.nttdata.etherwallet.model.http_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 16/11/2016.
 */

public class BaseResponse {

    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getStatus() {
        return status;
    }


    public void setStatus(Integer status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "BaseResponse{" +
                "status=" + status +
                '}';
    }
}
