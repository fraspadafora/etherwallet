package com.nttdata.etherwallet.model.http_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 17/11/2016.
 */

public class GasPriceResponse extends BaseResponse{

    @SerializedName("gasPrice")
    @Expose
    public Integer gasPrice;

    public Integer getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(Integer gasPrice) {
        this.gasPrice = gasPrice;
    }
}
