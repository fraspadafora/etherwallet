package com.nttdata.etherwallet.util.http_util;

/**
 * Created by Francesco on 16/11/2016.
 */

public interface UrlServerPath {

    String http   = "http://";
    String https  = "htps://";
    //String ip_ntt = "10.166.30.2:8082";
    //String ip     = "vt-spadaforafr.valueteam.com:8082";
    String ip_ntt = "http://168.128.12.89:8545";
    String default_ip = "168.128.12.89";
    String default_port = "8545";
    //String my_ip  = "192.168.1.102:8082";

    String SERVICE_BALANCE                   = "/balance";
    String SERVICE_ESTIMATE_GAS              = "/estimateGas";
    String SERVICE_NONCE                     = "/nonce";
    String SERVICE_GAS_PRICE                 = "/gasPrice";
    String SERVICE_SEND_ROW_TRANSACTION      = "/sendRawTransaction";
    String SERVICE_TRANSACTION_IN_BLOCKCHAIN = "/transactionBlock";


//    //IP FRANCISCO
//    String BALANCE_PATH_HTTP        = http+ip+SERVICE_BALANCE;
//    String ESTIMATE_GAS_PATH_HTTP   = http+ip+SERVICE_ESTIMATE_GAS;
//    String NONCE_PATH_HTTP          = http+ip+SERVICE_NONCE;
//    String GAS_PRICE_PATH_HTTP      = http+ip+SERVICE_GAS_PRICE;
//    String SEND_ROW_TRANSACTION_PATH_HTTP = http+ip+SERVICE_SEND_ROW_TRANSACTION;
//    String SEND_TRANSACTION_BLOCKCHAIN_PATH_HTTP = http+ip+SERVICE_TRANSACTION_IN_BLOCKCHAIN;

    //IP SERVER NTT

    String BALANCE_PATH_HTTP        = http+ip_ntt+SERVICE_BALANCE;
    String ESTIMATE_GAS_PATH_HTTP   = http+ip_ntt+SERVICE_ESTIMATE_GAS;
    String NONCE_PATH_HTTP          = http+ip_ntt+SERVICE_NONCE;
    String GAS_PRICE_PATH_HTTP      = http+ip_ntt+SERVICE_GAS_PRICE;
    String SEND_ROW_TRANSACTION_PATH_HTTP = http+ip_ntt+SERVICE_SEND_ROW_TRANSACTION;
    String SEND_TRANSACTION_BLOCKCHAIN_PATH_HTTP = http+ip_ntt+SERVICE_TRANSACTION_IN_BLOCKCHAIN;

    //MIO IP
//    String BALANCE_PATH_HTTP        = http+my_ip+SERVICE_BALANCE;
//    String ESTIMATE_GAS_PATH_HTTP   = http+my_ip+SERVICE_ESTIMATE_GAS;
//    String NONCE_PATH_HTTP          = http+my_ip+SERVICE_NONCE;
//    String GAS_PRICE_PATH_HTTP      = http+my_ip+SERVICE_GAS_PRICE;


}
