package com.nttdata.etherwallet.util.http_util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Francesco on 16/11/2016.
 */

 public enum ResultCode {

    RESPONSE_SUCCESSFUL(200),
    GENERIC_INTERNAL_ERROR(400),
    FUNCTION_EXCECUTION_ERROR(401),
    INTERNAL_SERVER_ERROR(402),
    TRANSACTION_NOT_INCLUDED_IN_A_BLOCK(403),
    INVALID_INPUT(404),
    TIMEOUT_ERROR(405),
    NETWORK_ERROR(406),
    MAPPING_CODE_ERROR(407),
    GENERIC_ERROR(415);



    private int resultCode;
    private static Map<Integer, ResultCode> map = new HashMap<Integer, ResultCode>();

    static {
        for (ResultCode resultCode : ResultCode.values()) {
            map.put(resultCode.resultCode, resultCode);
        }
    }

    public static ResultCode valueOf(int resultCode){
        return map.containsKey(resultCode)? map.get(resultCode) : MAPPING_CODE_ERROR;
    }

    ResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public int getResultCode() {
        return resultCode;
    }


}