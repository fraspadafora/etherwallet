package com.nttdata.etherwallet.util;

import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.ethereum.utils.Util;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Francesco on 13/12/2016.
 */

public class Utility {

    public static BigDecimal etherUnitConverter(int type, BigDecimal value, EtherUnit to){
        BigDecimal valueConverted = null;

        switch (type){
            case 0:
                valueConverted = Util.convert(value, EtherUnit.wei, to);
                break;
            case 1:
                valueConverted = Util.convert(value, EtherUnit.Kwei, to);
                break;
            case 2:
                valueConverted = Util.convert(value, EtherUnit.Mwei, to);
                break;
            case 3:
                valueConverted = Util.convert(value, EtherUnit.Gwei, to);
                break;
            case 4:
                valueConverted = Util.convert(value, EtherUnit.szabo, to);
                break;
            case 5:
                valueConverted = Util.convert(value, EtherUnit.finney, to);
                break;
            case 6:
                valueConverted = Util.convert(value, EtherUnit.ether, to);
                break;
            case 7:
                valueConverted = Util.convert(value, EtherUnit.Kether, to);
                break;
            case 8:
                valueConverted = Util.convert(value, EtherUnit.Mether, to);
                break;
            case 9:
                valueConverted = Util.convert(value, EtherUnit.Gether, to);
                break;
            case 10:
                valueConverted = Util.convert(value, EtherUnit.Tether, to);
                break;
        }
        return valueConverted.stripTrailingZeros();
    }

    public static EtherUnit getEtherUnitFromPosition(int position){
        EtherUnit unit = EtherUnit.wei;
        switch (position){
            case 1:
                unit = EtherUnit.Kwei;
                break;
            case 2:
                unit = EtherUnit.Mwei;
                break;
            case 3:
                unit = EtherUnit.Gwei;
                break;
            case 4:
                unit = EtherUnit.szabo;
                break;
            case 5:
                unit = EtherUnit.finney;
                break;
            case 6:
                unit = EtherUnit.ether;
                break;
            case 7:
                unit = EtherUnit.Kether;
                break;
            case 8:
                unit = EtherUnit.Mether;
                break;
            case 9:
                unit = EtherUnit.Gether;
                break;
            case 10:
                unit = EtherUnit.Tether;
                break;
        }
        return unit;
    }

    public static boolean isValidEtherAmount(String amount, int unitIndex){
        try {
            BigDecimal amountDecimal = new BigDecimal(amount);
            amountDecimal = etherUnitConverter(unitIndex, amountDecimal, EtherUnit.wei);
            if (!amountDecimal.add(new BigDecimal(amountDecimal.toBigInteger().negate())).equals(new BigDecimal("0"))) {
                return false;
            }
        }catch(Exception e){
            return false;
        }
        return true;
    }

    public static boolean isValidEtherAddress(String address){
        if(address != null || address.length() == 42){
           if(address.charAt(0)=='0' || address.charAt(1)=='x'){
               try {
                   new BigInteger(address.substring(2).toLowerCase(), 16);
                   return true;
               }
               catch (Exception ex) {
                   return false;
               }
           }
        }
        return false;
    }

}

