package com.nttdata.etherwallet.util.http_util;

/**
 * Created by Francesco on 18/11/2016.
 */

public class Constants {

    public static final String FILE_PARAM = "file";
    public static final String GAS_ESTIMATION_PARAM="gasEstimation";
    public static final String ID_PRAM = "id";
    public static final String NAME_PARAM = "name";
    public static final String TO_PARAM = "to";
    public static final String AMOUNT_PARAM = "amount";
    public static final String GAS_PRICE_PARAM = "gas_price_param";
    public static final String GAS_PARAM = "gas";
    public static final String PASSWORD_PARAM = "password";
    public static final String SIGNED_TRANSACTION_PARAM = "signedTransaction";
    public static final String TRANSACTIONS_LIST = "listTransaction";
    public static final String ADDRESS_PARAM = "address";
    public static final String NONCE_PARAM = "nonce";
    public static final String BALANCE_PARAM = "balance";
    public static final String ACCOUNT_PARAM = "account";
    public static final String DATA_PARAM = "data";

    public static final String TYPE_WALLET = "typeWallet";
    public static final String TYPE_LOGISTIC = "typeLogistic";

    public static final int DEFAULT_ESTIMATE_GAS = 21000;


    public static final String TRANSACTION_SUCCESS = "success";
    public static final String TRANSACTION_FAILURE = "failure";
    public static final String TYPE_PARAM = "type";
    public static final String TRANSACTION_TYPE = "transaction";
    public static final String ETHER_UNIT = "EtherUnit";
    public static final String ESTIMATE_FAILURE = "Estimate failure...";
    public static final String ETHER_UNIT_AMOUNT = "EtherUnitAmount";
    public static final String ETHER_UNIT_GAS_PRICE = "EtherUnitGasPrice";
    public static final String TXID_PARAM = "txId";
    public static final String LIST_TRANSACTION_PARAM = "transactionList";
    public static final String BLOCK_RESULT_PARAM = "blockResultParam";
    public static final String TRANSACTION_PARAM = "transaction";
    public static final String ACCOUNT_IMP_PARAM = "accountParam";
    //public static final String IP_DEFAULT = "http://10.166.30.2:8082/";
    public static final String IP_DEFAULT  = "http://168.128.12.89:8545";
    public static long GAS_ESTIMATION_LIMIT = 50000000;

    public static String IP_SELECTED;

    public static final String[] ETHET_UNIT_LIST_TYPE = {"Wei", "Kwei", "Mwei", "Gwei", "Szabo", "Finney", "Ether", "Kether", "Gether", "Tether", "Mether"};

    public static String getIpSelected() {
        return IP_SELECTED;
    }

    public static void setIpSelected(String ipSelected) {
        IP_SELECTED = ipSelected;
    }
}
