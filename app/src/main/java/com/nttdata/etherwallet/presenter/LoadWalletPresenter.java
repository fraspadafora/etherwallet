package com.nttdata.etherwallet.presenter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.nttdata.ethereum.utils.EtherUnit;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.ethereum.EthereumManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.ip_model.IP;
import com.nttdata.etherwallet.presenter.callback.LoadWalletCallBack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by FarandaAl on 08/11/2016.
 */

public class LoadWalletPresenter {

    private final String TAG = LoadWalletPresenter.class.getSimpleName();

    private final Context mContext;
    private final LoadWalletCallBack mCallback;
    private final StorageManager mStorage;
    private final EthereumManager mEthereum;
    private View rootView;

    private String name;

    public LoadWalletPresenter(Context context, LoadWalletCallBack callback, View rootView){
        mContext = context;
        mStorage = StorageManager.getInstance(context);
        mEthereum = EthereumManager.getInstance(context);
        mCallback = callback;

        this.rootView = rootView;
    }

    public void loadAllWallet(){
        try {
            Log.d(TAG, "loadAllWallet");
            // recupera ic_wallet selezionato o salvati
            if(isWalletSelected()){

                mCallback.onDoneOnce(mStorage.getSelectedEtherAccount());
            }else if(thereAreWallet()){
                mCallback.onDoneList((List<EtherAccount>) mStorage.getAll(EtherAccount.class));
            }else{
                mCallback.onDoneList(Collections.<EtherAccount>emptyList());
            }
            mCallback.manageProgressBar(false);
        }catch (Exception e){
            Log.e(TAG, "loadAllWallet exception", e);
            mCallback.onError();
            mCallback.manageProgressBar(false);
        }
    }

    public void saveWalletData(Intent data){
        mCallback.manageProgressBar(true);
        try {
            InputStream inputStream = mContext.getContentResolver().openInputStream(data.getData());

            String file = extractFileString(inputStream);

            EtherAccount account = new Gson().fromJson(file, EtherAccount.class);
            if(account != null) {

                if (name != null) {
                    account.setName(name);
                }

                account.setAddress("0x" + account.getAddress());
                account.setFile(file);

                mStorage.saveObject(account);
                loadAllWallet();
            }

        }catch (Exception e){
            Log.e(TAG, "onActivityResult error Exception", e);
            Snackbar.make(rootView, "Sava account failure", Snackbar.LENGTH_LONG).show();
            mCallback.onError();
            mCallback.manageProgressBar(false);
        }
    }

    public String extractFileString(InputStream in){
        StringBuilder sb = null;
        try {

            BufferedReader r = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            String str = null;
            sb = new StringBuilder();
            while ((str = r.readLine()) != null) {
                sb.append(str);
            }
            System.out.println("data from InputStream as String : " + sb.toString());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return sb.toString();
    }

    public void createEditDialog(final FrameLayout contentMainWallet, final Intent data){
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
            alert.setTitle(R.string.account_creation);

            Context context = rootView.getContext();
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);

            final EditText inputName = new EditText(mContext);
            inputName.setHint("Name");
            inputName.setInputType(InputType.TYPE_CLASS_TEXT);
            inputName.setRawInputType(Configuration.UI_MODE_TYPE_NORMAL);
            layout.addView(inputName);

            alert.setView(layout);
            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (inputName.getText() != null) {
                        setName(inputName.getText().toString());
                        saveWalletData(data);
                    } else {
                        Snackbar.make(contentMainWallet, R.string.no_save, Snackbar.LENGTH_LONG).show();
                    }

                }
            });
            alert.setNegativeButton(("cancel"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    setName(null);
                }
            });
            alert.show();
        }catch (Exception e){
            Log.d(TAG, "createEditDialog: ");
            e.printStackTrace();
        }
    }

    private boolean thereAreWallet() {
        return mStorage.checkForElements(EtherAccount.class);
    }

    public boolean isWalletSelected(){
        return mStorage.isSelectedEtherAccount();
    }

    public void setSelectedAccount(String accountId){
        mStorage.setSelectedEtherAccount(accountId);
    }

    public EtherAccount getSelectedAccount(){
        return mStorage.getSelectedEtherAccount();
    }

    public List<EtherAccount> getAccountsList(){
        return (List<EtherAccount>) mStorage.getAll(EtherAccount.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
       this.name = name;
    }

}