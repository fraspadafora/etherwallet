package com.nttdata.etherwallet.presenter.http_presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.nttdata.etherwallet.manager.http_request.VolleyManager;
import com.nttdata.etherwallet.presenter.callback.http_callback.VolleyCallBack;

import org.json.JSONObject;

/**
 * Created by Francesco on 16/11/2016.
 */

public class VolleyPresenter {
    public static final String TAG = VolleyPresenter.class.getSimpleName();

    private VolleyCallBack mCallback;
    private VolleyManager mManager;
    private final Gson gson = new Gson();

    //response parameters

    private Integer blockNumber;
    private String blockHash;


    public VolleyPresenter(Context context, VolleyCallBack mCallback) {
        this.mCallback = mCallback;
        mManager = VolleyManager.getInstance(context);
    }


//    public void transactionInBlockChain(String txId){
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    Log.d(TAG, " transactionInBlockChain success: ");
//                    BlockChainTransactionResponse blockChainTRansactionResponse = gson.fromJson(response.toString(), BlockChainTransactionResponse.class);
//                    blockNumber = blockChainTRansactionResponse.getBlockNumber();
//                    blockHash = blockChainTRansactionResponse.getBlockHash();
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "transactionInBlockChain failure: ");
//                    blockNumber = -1;
//                    blockHash = null;
//                }
//            };
//            BlockChainTransactionRequest blockChain = new BlockChainTransactionRequest();
//            blockChain.setTxId(txId);
//            String params = gson.toJson(blockChain);
//            JSONObject request = new JSONObject(params);
//            mManager.generatePost(UrlServerPath.BALANCE_PATH_HTTP, request, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "transactionInBlockChain: EXCEPTION");
//            e.printStackTrace();
//            blockNumber = -1;
//            blockHash = null;
//        }
//    }


    public String getBlockHash() {
        return blockHash;
    }

    public Integer getBlockNumber() {
        return blockNumber;
    }

}