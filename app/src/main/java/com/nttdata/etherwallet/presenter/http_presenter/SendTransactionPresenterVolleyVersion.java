//package com.nttdata.etherwallet.presenter.http_presenter;
//
//import android.content.Context;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.util.Log;
//
//import com.android.volley.Request;
//import com.google.gson.Gson;
//import com.nttdata.ethereum.utils.Util;
//import com.nttdata.etherwallet.R;
//import com.nttdata.etherwallet.manager.data_base.StorageManager;
//import com.nttdata.etherwallet.manager.ethereum.EthereumManager;
//import com.nttdata.etherwallet.manager.http_request.VolleyManager;
//import com.nttdata.etherwallet.model.EtherAccount;
//import com.nttdata.etherwallet.model.http_request.BlockChainTransactionRequest;
//import com.nttdata.etherwallet.model.http_request.EstimateGasRequest;
//import com.nttdata.etherwallet.model.http_request.NonceRequest;
//import com.nttdata.etherwallet.model.http_request.SendRawTransactionRequest;
//import com.nttdata.etherwallet.model.http_response.DataResponse;
//
//import com.nttdata.etherwallet.model.transaction.Transaction;
//import com.nttdata.etherwallet.presenter.callback.http_callback.SendTransactionCallBack;
//import com.nttdata.etherwallet.util.http_util.Constants;
//import com.nttdata.etherwallet.util.http_util.ResultCode;
//import com.nttdata.etherwallet.util.http_util.UrlServerPath;
//import org.json.JSONObject;
//
//import java.math.BigDecimal;
//import java.math.BigInteger;
//import java.util.HashMap;
//import java.util.List;
//
//import io.realm.Realm;
//import io.realm.RealmList;
//
///**
// * Created by Francesco on 21/11/2016.
// */
//
//public class SendTransactionPresenterVolleyVersion{
//
//    private static final String TAG = SendTransactionPresenterVolleyVersion.class.getSimpleName();
//
////    private final OnListFragmentInteractionListener mListener;
//
//    private SendTransactionCallBack mCallBack;
//    private StorageManager mStorageManager;
//    private EthereumManager mEthereum;
//    private Context mContext;
//
//    private VolleyManager mVolleyManager;
//
//    private final Gson gson = new Gson();
//
//    private int gasEstimation = -1;
//    private Double gasPrice = null;
//    private Integer nonce = -1;
//    private String txId;
//    private BigInteger blockNumber;
//    private String blockHash;
////    private final long WAIT_TIME = 180000;
////    private boolean stopBlockTransaction;
////    private boolean blockTransactionOk;
//
//    private long elapsed;
//    private final static long INTERVAL=1000;
//    private final static long TIMEOUT=300000;
////    private final static long TIMEOUT=30000;
//
//    public SendTransactionPresenterVolleyVersion(Context context, SendTransactionCallBack mCallBack/*, OnListFragmentInteractionListener listener*/) {
//        this.mCallBack = mCallBack;
//        mVolleyManager = VolleyManager.getInstance(context);
//        mStorageManager = StorageManager.getInstance(context);
//        mEthereum = EthereumManager.getInstance(context);
//        mContext = context;
//
////        mListener = listener;
//    }
//
//    public void estimateGasAndSendTxCall(String from, String to, String amt, Integer nonce, String data){
//        double amount = Double.parseDouble(amt);
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    DataResponse dataResponse = gson.fromJson(response, DataResponse.class);
//                    gasEstimation = dataResponse.getData().getGasEstimation()+100000;
//                    Log.d(TAG, " estimateGas success: "+gasEstimation);
//                    mCallBack.setDoTransaction(true);
//                    mCallBack.updateGasAdvanced(String.valueOf(gasEstimation));
//                    mCallBack.calculateFreeEstimation(gasEstimation);
//                    mCallBack.checkGasEstimationValue(gasEstimation);
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "estimateGas failure: "+ error.toString());
//                    mCallBack.showSnackBar(Constants.ESTIMATE_FAILURE);
//                    gasEstimation = -1;
//                }
//            };
//
//            EstimateGasRequest estimateGas = new EstimateGasRequest();
//            estimateGas.setFrom(from);
//            estimateGas.setTo(to);
//            estimateGas.setValue(amount);
//            estimateGas.setNonce(nonce);
//
//            if(data != null) {
//                estimateGas.setData(data);
//            }
////            if(gas != null) {
////                estimateGas.setGas(gas);
////            }
////            if(gasPrice != null) {
////                estimateGas.setGasPrice(gasPrice);
////            }
//
//            String params = gson.toJson(estimateGas);
//            JSONObject request = new JSONObject(params);
//            mVolleyManager.generateRequest(Request.Method.POST, UrlServerPath.ESTIMATE_GAS_PATH_HTTP, request, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "estimateGas: EXCEPTION");
//            e.printStackTrace();
//            gasEstimation = -1;
//        }
//    }
//
//    public void gasPriceServerCall(){
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    DataResponse dataResponse = gson.fromJson(response, DataResponse.class);
//                    gasPrice = dataResponse.getData().getGasPrice();
//                    Log.d(TAG, " gasPrice success: "+gasPrice);
//                    mCallBack.updateGasPriceEditText(String.valueOf(gasPrice));
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "gasPrice failure: ");
//                    mCallBack.showSnackBar("Estimate failure...");
//                    mCallBack.stopProgressBar();
//                    gasPrice = null;
//                }
//            };
//            mVolleyManager.generateRequest(Request.Method.GET, UrlServerPath.GAS_PRICE_PATH_HTTP, null, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "gasPrice: EXCEPTION");
//            e.printStackTrace();
//        }
//    }
//
//
//    public void nonceServerCall(final String address, final String to, final String amount, final String data){
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    DataResponse dataResponse = gson.fromJson(response, DataResponse.class);
//                    nonce = dataResponse.getData().getNonce();
//                    Log.d(TAG, "nonce success: "+ nonce);
//                    estimateGasAndSendTxCall(address, to, amount, nonce, data);
//                    mCallBack.stopProgressBar();
//
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "nonce failure: "+ error.toString());
//                    mCallBack.showSnackBar("Estimate failure...");
//                    mCallBack.stopProgressBar();
//                }
//            };
//            NonceRequest nonce = new NonceRequest();
//            nonce.setAddress(address);
//            String params = gson.toJson(nonce);
//            JSONObject request = new JSONObject(params);
//            mVolleyManager.generateRequest(Request.Method.POST, UrlServerPath.NONCE_PATH_HTTP, request, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "nonce: EXCEPTION");
//            e.printStackTrace();
//            nonce = -1;
//        }
//    }
//
//    public void sendTransactionInBlock(final String txId, final String address, final List<Transaction> list/*, final EtherAccount account*/, final HashMap<String, Object> mapParamenters){
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    Log.d(TAG, " sendRowInTransaction success: "+ code.toString());
//
//                    final RealmList<Transaction> transactions = convertListToRealmList(list);
//                    DataResponse dataRespnse = gson.fromJson(response, DataResponse.class);
//                    blockNumber = dataRespnse.getData().getBlockNumber();
//                    blockHash = dataRespnse.getData().getBlockHash();
//                    Transaction tr = findTransaction(txId, transactions);
////                    Transaction tr = null;
////                    if(list != null && list.size()==0){
////                        tr = createTransaction(mapParamenters);
////                        transactions.add(tr);
////                    }else {
////                        tr = findTransaction(txId, transactions);
////                    }
//                    if(tr != null){
//                        mCallBack.showSnackBar(mContext.getString(R.string.transaction_in_block));
//                        mStorageManager.updateTransactionInBlock(tr, transactions, true, String.valueOf(blockNumber), address);
//                        mCallBack.updateListTransactions(true);
//                    }
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "sendRowInTransaction failure: "+ error);
////                    final RealmList<Transaction> transactions = convertListToRealmList(list);
////                    Transaction tr = findTransaction(txId, transactions);
////                    if(tr != null){
////                        if(!tr.isTimer()) {
////                            mStorageManager.setTimerTransaction(tr, true, account);
////                            startTimer();
////                        }
////                    }
//                    sendTransactionInBlock(txId, address, list, mapParamenters);
//                    mCallBack.stopProgressDialog();
//                }
//            };
//
//            BlockChainTransactionRequest blockTransaction = new BlockChainTransactionRequest();
//            blockTransaction.setTxId(txId);
//            String params = gson.toJson(blockTransaction);
//            JSONObject request = new JSONObject(params);
//            mVolleyManager.generateRequest(Request.Method.POST, UrlServerPath.SEND_TRANSACTION_BLOCKCHAIN_PATH_HTTP, request, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "sendRowInTransaction: EXCEPTION");
//            e.printStackTrace();
//        }
//    }
//
////    public void startTimer() {
////        try {
////            TimerTask task = new TimerTask() {
////                @Override
////                public void run() {
////                    elapsed += INTERVAL;
////                    if (elapsed >= TIMEOUT) {
////                        this.cancel();
////                        mCallBack.updateListTransactions(false);
////
////                    }
////                }
////            };
////
////            Timer timer = new Timer();
////            timer.scheduleAtFixedRate(task, INTERVAL, INTERVAL);
////        }catch (Exception e){
////            e.printStackTrace();
////        }
////    }
//
////    public void startCountDown(){
////
////        final CountDownTimer countDown = new CountDownTimer(WAIT_TIME, 1000) {
////            @Override
////            public void onTick(long millisUntilFinished) {
////                //nothing
////            }
////
////            @Override
////            public void onFinish() {
////                Log.d(TAG, "onFinish counter block transaction");
////                stopBlockTransaction = true;
////                this.cancel(); bhb
////            }
////        }.start();
////    }
//
//
//
//    public void sendRowInTransaction(final HashMap<String, Object> mapParameters, final EtherAccount account){
//        String signedTransaction = (String) mapParameters.get(Constants.SIGNED_TRANSACTION_PARAM);
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    Log.d(TAG, " sendRowInTransaction success: "+ code.toString());
//                    mCallBack.stopProgressBar();
//                    DataResponse dataResponse = gson.fromJson(response, DataResponse.class);
//
//                    txId = dataResponse.getData().getTxId();
//
//                    manageTransactionsBlock(mapParameters, account);
//                    mCallBack.showSnackBar(mContext.getString(R.string.transaction_ok));
//                    mCallBack.stopProgressDialog();
//                    mCallBack.updateAccountAfterLogisticTransaction(Constants.TRANSACTION_SUCCESS, account);
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "sendRowInTransaction failure: "+ error.toString()+ " code: "+code.toString());
//                    mCallBack.stopProgressDialog();
//                    mCallBack.showSnackBar(mContext.getString(R.string.error_transaction));
//                    mCallBack.updateAccountAfterLogisticTransaction(Constants.TRANSACTION_FAILURE, account);
//                    txId = null;
//                }
//            };
//
//            SendRawTransactionRequest srTransaction = new SendRawTransactionRequest();
//            srTransaction.setSignedTransaction(signedTransaction);
//            String params = gson.toJson(srTransaction);
//            JSONObject request = new JSONObject(params);
//            mVolleyManager.generateRequest(Request.Method.POST, UrlServerPath.SEND_ROW_TRANSACTION_PATH_HTTP, request, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "sendRowInTransaction: EXCEPTION");
//            e.printStackTrace();
//        }
//    }
//
//    public Transaction createTransaction(HashMap<String, Object> mapParameters){
//        Transaction tr = null;
//        String amount = (String) mapParameters.get(Constants.AMOUNT_PARAM);
//        String to = (String) mapParameters.get(Constants.TO_PARAM);
//        String address = (String) mapParameters.get(Constants.ADDRESS_PARAM);
//        String gas = String.valueOf(mapParameters.get(Constants.GAS_PARAM));
//        String gasPrice = String.valueOf(mapParameters.get(Constants.GAS_PRICE_PARAM));
//
//        tr = new Transaction(address, to, amount, txId, false, null, null, gas, gasPrice);
//
//        return tr;
//    }
//
//    //TODO da rifattorizzare nello storage manager
//    public void manageTransactionsBlock(final HashMap<String, Object> mapParameters, EtherAccount account){
//
//        Realm realm = mStorageManager.getDefaultInstance();
//        realm.beginTransaction();
//        try {
//            String address = (String) mapParameters.get(Constants.ADDRESS_PARAM);
//            Transaction tr = realm.copyToRealmOrUpdate(createTransaction(mapParameters));
//            RealmList<Transaction> list = mStorageManager.getTransactionsList(address);
//            list.add(tr);
//
//            for (int i = 0; i < list.size(); i++) {
//                Transaction tempTr = list.get(i);
//                if (!list.get(i).isValidate()) {
//                    sendTransactionInBlock(tempTr.getTxId(), tempTr.getAddress(), list/*, account*/, mapParameters);
//                }
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        realm.commitTransaction();
//    }
//
//    public boolean isOnline() {
//        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        return netInfo != null && netInfo.isConnectedOrConnecting();
//    }
//
//    /**
//     *
//     * @param password
//     * @param to
//     * @param nonce
//     * @param amount
//     * @param gasPrice
//     * @param estimateGas
//     * @param account
//     * @return
//     */
//    public String generateSignedTransaction(String password, String to, int nonce, BigDecimal amount,  double gasPrice, int estimateGas, EtherAccount account, String data){
//        String signedTransaction = null;
//        try{
//            BigInteger nonceInt = new BigInteger(String.valueOf(nonce));
//            BigInteger amountInt = amount.toBigInteger();
//            BigInteger gasPriceInt  = new BigInteger(String.valueOf(Math.round(gasPrice)));
//            BigInteger gasLimitInt  = new BigInteger(String.valueOf(estimateGas));
//
//            String file = account.getFile();
//            //TODO aggiungere selezione account from account manager - eliminare string file
//
//            //WITH PRIVATE KEY
//            byte[] privateKey = mEthereum.generatePrivateKey(file, password);
//            if(data == null){
//                data = "";
//            }
//            signedTransaction = mEthereum.createSignedTransactionWithPrivateKey(privateKey, to, amountInt, nonceInt, gasPriceInt, gasLimitInt, Util.hexStringToByteArray(data));
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//        return signedTransaction;
//    }
//
////    public List<Transaction> convertRealmListToList(RealmList<Transaction> realmList){
////        List<Transaction> list = new ArrayList<>();
////        try {
////            for (int i = 0; i < realmList.size(); i++) {
////                list.add(realmList.get(i));
////            }
////        }catch (Exception e){
////            e.printStackTrace();
////        }
////        return list;
////    }
//
//    public RealmList<Transaction> convertListToRealmList(List<Transaction> list){
//        RealmList<Transaction> realmList = new RealmList<>();
//        for(int i=0; i<list.size(); i++){
//            realmList.add(list.get(i));
//        }
//        return realmList;
//    }
//
//    public Transaction findTransaction(String txId, List<Transaction> transactions){
//        Transaction tr = null;
//        try {
//
//            for (int i = 0; i < transactions.size(); i++) {
//                if (transactions.get(i).getTxId().equals(txId)) {
//                    tr = transactions.get(i);
//                    break;
//                }
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return tr;
//    }
//
//
//        public Integer getNonce() {
//        return nonce;
//    }
//
//    public Double getGasPrice() {
//        return gasPrice;
//    }
//
//    public void setGasPrice(Double gasPrice) {
//        this.gasPrice = gasPrice;
//    }
//
//    public int getGasEstimation() {
//        return gasEstimation;
//    }
//
//}