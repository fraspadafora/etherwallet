package com.nttdata.etherwallet.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.zxing.WriterException;
import com.nttdata.etherwallet.manager.qr.QrCodeManager;
import com.nttdata.etherwallet.presenter.callback.QrCodeCallBack;

/**
 * Created by Francesco on 15/11/2016.
 */

public class QrCodePresenter {
    public static final String TAG = QrCodePresenter.class.getSimpleName();

    private final QrCodeCallBack mCallback;
    private QrCodeManager mQrCode;
    private Context mContext;


    public QrCodePresenter(Context context, String data, QrCodeCallBack callback){
        Log.d(TAG, "QrCodePresenter: "+ TAG);
        mContext = context;
        mQrCode = QrCodeManager.getInstance(data);
        mCallback = callback;
    }

    public Bitmap generateQrCode(){
        Log.d(TAG, "generateQrCode: "+TAG);
        Bitmap bitmap = null;
        try {
            bitmap = mQrCode.encodeAsBitmap();
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public String readQrCode(){
        return "";
    }

}