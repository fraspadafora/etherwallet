package com.nttdata.etherwallet.presenter.callback.http_callback;

import com.nttdata.etherwallet.model.EtherAccount;

import java.math.BigInteger;

/**
 * Created by Francesco on 21/11/2016.
 */

public interface SendTransactionCallBack {

    void updateListTransactions(boolean success);
    void updateGasPriceEditText(String gasPrice);
    void updateGasAdvanced(String gas);
    void setDoTransaction(boolean state);
    void stopProgressBar();
    void showSnackBar(String message);
    void updateAccountAfterLogisticTransaction(String s, EtherAccount account);
    void stopProgressDialog();
    void calculateFreeEstimation(BigInteger gasEstimation);
    void checkGasEstimationValue(BigInteger gasEstimation);
//    void startTimer();
}
