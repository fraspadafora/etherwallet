package com.nttdata.etherwallet.presenter.callback.http_callback;

/**
 * Created by Francesco on 17/11/2016.
 */

public interface SelectFragmentCallBack {
    void updateAccountList();
    void serverUnreachable();
}
