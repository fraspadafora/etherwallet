package com.nttdata.etherwallet.presenter.callback;

import java.math.BigInteger;

/**
 * Created by Francesco on 14/12/2016.
 */

public interface DetailWalletCallBack {
    void updateBalanceValue(BigInteger balance);
    void updateListTransactions(boolean success);
}
