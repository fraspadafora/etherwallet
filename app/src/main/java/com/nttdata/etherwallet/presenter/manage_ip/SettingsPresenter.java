package com.nttdata.etherwallet.presenter.manage_ip;

import android.content.Context;

import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.model.ip_model.IP;
import com.nttdata.etherwallet.presenter.callback.ip.SettingsCallBack;

import io.realm.RealmList;

/**
 * Created by Francesco on 20/12/2016.
 */

public class SettingsPresenter {
    private final String TAG = SettingsPresenter.class.getSimpleName();

    private Context mContext;
    private SettingsCallBack mCallback;
    private StorageManager mStorageManager;

    public SettingsPresenter(Context mContext, SettingsCallBack mCallback) {
        this.mContext = mContext;
        this.mCallback = mCallback;
        mStorageManager = StorageManager.getInstance(mContext);
    }

    public void addIpToAccount(EtherAccount accSelected, String ip, String port) {
        if(accSelected != null){
            mStorageManager.addNewIP(accSelected, ip, port);
        }else{
            mCallback.showMessageAccount();
        }
    }
}
