package com.nttdata.etherwallet.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.nttdata.etherwallet.manager.async_task.AsincTaskServerCall;
import com.nttdata.etherwallet.manager.async_task.AsynkTaskTransactionBlockManager2;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.model.transaction.Transaction;
import com.nttdata.etherwallet.presenter.callback.DetailWalletCallBack;
import com.nttdata.etherwallet.presenter.http_presenter.SelectFragmentPresenter;
import com.nttdata.etherwallet.util.http_util.Constants;

import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by Francesco on 14/12/2016.
 */

public class DetailWalletPresenter {

    private static final String TAG = SelectFragmentPresenter.class.getSimpleName();

    private DetailWalletCallBack mCallBack;
    private Context mContext;
    //-----------------------------------------------
    private StorageManager mStorageManager;
    //-----------------------------------------------

    //response parameters
    private BigInteger balance;

    //private final Gson gson = new Gson();

    public DetailWalletPresenter(Context context, DetailWalletCallBack callBack) {
        this.mContext = context;
        this.mCallBack = callBack;
        //-----------------------------------------------------------
        mStorageManager = StorageManager.getInstance(mContext);
        //-----------------------------------------------------------
    }

//    public void balanceRequest(final String address){
//        Log.d(TAG, "balanceRequest: ");
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    Log.d(TAG, " balaceRequest success: ");
//
//                    try {
//                        DataResponse dataResponse = gson.fromJson(response, DataResponse.class);
//                        balance = dataResponse.getData().getBalance();
//                        mStorageManager.updateBalance(address, balance);
//                        mCallBack.updateBalanceValue(balance);
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "balaceRequest failure: ");
//                    balanceRequest(address);
//                }
//            };
//
//            BalanceRequest balance = new BalanceRequest();
//            balance.setAddress(address);
//            String params = gson.toJson(balance);
//            JSONObject request = new JSONObject(params);
//
//            mVolleyManager.generateRequest(Request.Method.POST, UrlServerPath.BALANCE_PATH_HTTP, request, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "balaceRequest: EXCEPTION");
//            e.printStackTrace();
//        }
//    }

    public void balanceRequest(String address){
        Log.d(TAG, "balanceRequest: START");
        try {
            AsincTaskServerCall asynk = new AsincTaskServerCall(address, balance, mContext, mCallBack, null);
            asynk.execute();
        }catch (Exception e){
            Log.d(TAG, "balaceRequest: EXCEPTION");
            balanceRequest(address);
            e.printStackTrace();
        }
    }

    public BigInteger getBalance() {
        return balance;
    }

    //----------------------------------------------

    private boolean validationCheckStatus = false;

    public void checkTransactionsValidation(String address){
        Log.i(TAG, "checkTransactionsValidation");
        validationCheckStatus = true;
        RealmList<Transaction> list = mStorageManager.getTransactionsList(address);
        for (int i = 0; i < list.size(); i++) {
            Transaction tempTr = list.get(i);
            if (!list.get(i).isValidate()) {
                createAsynkInBlock(tempTr, list);
            }
        }
    }

    public void stopCheckTransactionsValidation(){
        Log.i(TAG, "stopCheckTransactionsValidation");
        validationCheckStatus = false;
    }

    public void createAsynkInBlock(Transaction tempTr, List<Transaction> list){
        Log.i(TAG, "createAsynkInBlock. Transaction: "+tempTr.getTxId());
        AsynkTaskTransactionBlockManager2 asynkBlockTransaction = new AsynkTaskTransactionBlockManager2(this);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Constants.TXID_PARAM, tempTr.getTxId());
        map.put(Constants.ADDRESS_PARAM, tempTr.getAddress());
        map.put(Constants.LIST_TRANSACTION_PARAM, list);
        map.put(Constants.TRANSACTION_PARAM, tempTr);
        asynkBlockTransaction.execute(map);
    }

    public void sendTransactionInBlock(HashMap<String, Object> mapParamenters){
        Log.i(TAG, "sendTransactionInBlock: START");
        if(validationCheckStatus) {
            List<Transaction> list = null;
            Transaction transaction = null;
            try {
                //TODO: gestire il caso in cui la lista delle transazioni sia nulla e di conseguenza sarà nulla la transazione ottenuta(nel caso di transazione proveniente dal marketplace)
                if (mapParamenters != null && mapParamenters.get(Constants.BLOCK_RESULT_PARAM) != null) {
                    EthGetTransactionReceipt blockResult = (EthGetTransactionReceipt) mapParamenters.get(Constants.BLOCK_RESULT_PARAM);
                    if (blockResult.getTransactionReceipt() != null) {
                        String address = mapParamenters.get(Constants.ADDRESS_PARAM).toString();
                        list = mStorageManager.getTransactionsList(address);
                        final RealmList<Transaction> transactions = convertListToRealmList(list);
                        BigInteger blockNumber = blockResult.getTransactionReceipt().getBlockNumber();
                        String txId = (String) mapParamenters.get(Constants.TXID_PARAM);
                        Transaction tr = findTransaction(txId, transactions);
                        if (tr != null) {
                            Log.i(TAG, "Transaction Validated. TX: "+txId);
                            mStorageManager.updateTransactionInBlock(tr, transactions, true, blockNumber.toString(), address);
                            mCallBack.updateListTransactions(true);
                        }
                    } else {
                        transaction = (Transaction) mapParamenters.get(Constants.TRANSACTION_PARAM);
                        createAsynkInBlock(transaction, list);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "sendRowInTransaction: EXCEPTION",e);
            }
        }
    }

    public RealmList<Transaction> convertListToRealmList(List<Transaction> list){
        RealmList<Transaction> realmList = new RealmList<>();
        for(int i=0; i<list.size(); i++){
            realmList.add(list.get(i));
        }
        return realmList;
    }

    public Transaction findTransaction(String txId, List<Transaction> transactions){
        Transaction tr = null;
        try {

            for (int i = 0; i < transactions.size(); i++) {
                if (transactions.get(i).getTxId().equals(txId)) {
                    tr = transactions.get(i);
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tr;
    }

    //----------------------------------------------

}