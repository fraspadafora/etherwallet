package com.nttdata.etherwallet.presenter.http_presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;

import com.nttdata.ethereum.utils.Util;
import com.nttdata.etherwallet.R;
import com.nttdata.etherwallet.manager.async_task.AsynkTaskEstimateGas;
import com.nttdata.etherwallet.manager.async_task.AsynkTaskTransactionBlockManager;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import com.nttdata.etherwallet.manager.ethereum.EthereumManager;
import com.nttdata.etherwallet.manager.http_request.Web3JManager;
import com.nttdata.etherwallet.model.EtherAccount;
import com.nttdata.etherwallet.model.transaction.Transaction;
import com.nttdata.etherwallet.presenter.callback.http_callback.SendTransactionCallBack;
import com.nttdata.etherwallet.util.http_util.Constants;

import org.web3j.protocol.core.methods.response.EthEstimateGas;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by Francesco on 21/11/2016.
 */

public class SendTransactionPresenter{

    private static final String TAG = SendTransactionPresenter.class.getSimpleName();

    private SendTransactionCallBack mCallBack;
    private StorageManager mStorageManager;
    private EthereumManager mEthereum;
    private Context mContext;

    private Web3JManager mWeb3jManager;

    private BigInteger gasEstimation;
    private BigInteger gasPrice;
    BigInteger nonce;
    private String txId;
    private BigInteger blockNumber;
    private String blockHash;

    private boolean transactionInBlockSuccess;

    public SendTransactionPresenter(Context context, SendTransactionCallBack mCallBack) {
        this.mCallBack = mCallBack;
        if(Constants.IP_SELECTED!= null && Constants.IP_SELECTED != "") {
            mWeb3jManager = Web3JManager.getInstance(Constants.IP_SELECTED);
        }else{
            mWeb3jManager = Web3JManager.getInstance(Constants.IP_DEFAULT);
        }
        mStorageManager = StorageManager.getInstance(context);
        mEthereum = EthereumManager.getInstance(context);
        mContext = context;
    }

    public void estimateGasCall(String from, String to, String amt, String data){
        Log.d(TAG, "estimateGasAndSendTxCall: START");
        try {
            AsynkTaskEstimateGas asynkTaskEstimateGas = new AsynkTaskEstimateGas(from,to,amt, data, mCallBack, mWeb3jManager);
            asynkTaskEstimateGas.execute();
        }catch (Exception e){
            Log.e(TAG, "estimateGas: EXCEPTION", e);
            mCallBack.showSnackBar(Constants.ESTIMATE_FAILURE);
        }
    }

    public void estimateGasAndSendTxCall(String from, String to, String amt, BigInteger nonce, String data){
        Log.d(TAG, "estimateGasAndSendTxCall: START");
        try {
            BigInteger amount = new BigInteger(amt);
            EthEstimateGas eGas = mWeb3jManager.getEstimateGas(from, to, amount, nonce, data);
            gasEstimation = eGas.getAmountUsed();
            mCallBack.setDoTransaction(true);
            mCallBack.updateGasAdvanced(String.valueOf(gasEstimation));
            mCallBack.calculateFreeEstimation(gasEstimation);
            mCallBack.checkGasEstimationValue(gasEstimation);

        }catch (Exception e){
            Log.d(TAG, "estimateGas: EXCEPTION");
            e.printStackTrace();
            mCallBack.stopProgressBar();
            mCallBack.showSnackBar(Constants.ESTIMATE_FAILURE);
        }
    }

    public void gasPriceServerCall(){
        Log.d(TAG, "gasPriceServerCall: START");
        try {
            EthGasPrice gPrice = mWeb3jManager.getGasPrice();
            gasPrice = gPrice.getGasPrice();
            String gasPriceString = gasPrice.toString();
            mCallBack.updateGasPriceEditText(gasPriceString);
        }catch (Exception e){
            Log.d(TAG, "gasPrice: EXCEPTION");
            e.printStackTrace();
        }
    }

    public boolean checkCameraPermission(Context context) {
        String permission = "android.permission.CAMERA";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    public void nonceServerCall(final String address, final String to, final String amount, final String data){
        Log.d(TAG, "nonceServerCall: start");
        try {
            EthGetTransactionCount ethgetTransaction = mWeb3jManager.getNonce(address);
            nonce = ethgetTransaction.getTransactionCount();
            estimateGasAndSendTxCall(address, to, amount, nonce, data);
            mCallBack.stopProgressBar();
        } catch (Exception e) {
            Log.d(TAG, "nonceServerCall: ");
            e.printStackTrace();
        }
    }

    public void sendRowInTransaction(final HashMap<String, Object> mapParameters, final EtherAccount account){
        Log.d(TAG, "sendRowInTransaction: START");
        String signedTransaction = (String) mapParameters.get(Constants.SIGNED_TRANSACTION_PARAM);
        try {
            EthSendTransaction request = mWeb3jManager.runRawTransaction(signedTransaction);
            mCallBack.stopProgressBar();
            txId = request.getTransactionHash();
            mCallBack.stopProgressDialog();
            manageTransactionsBlock(mapParameters/*, account*/);
            mCallBack.showSnackBar(mContext.getString(R.string.transaction_ok));
            mCallBack.updateAccountAfterLogisticTransaction(Constants.TRANSACTION_SUCCESS, account);

        }catch (Exception e){
            Log.d(TAG, "sendRowInTransaction: EXCEPTION");
            e.printStackTrace();
        }
    }


    public void sendTransactionInBlock(HashMap<String, Object> mapParamenters){
        Log.d(TAG, "sendTransactionInBlock: START");
        List<Transaction> list = null;
        Transaction transaction = null;
        try {
            //TODO: gestire il caso in cui la lista delle transazioni sia nulla e di conseguenza sarà nulla la transazione ottenuta(nel caso di transazione proveniente dal marketplace)
            if(mapParamenters != null && mapParamenters.get(Constants.BLOCK_RESULT_PARAM)!=null) {
                EthGetTransactionReceipt blockResult = (EthGetTransactionReceipt) mapParamenters.get(Constants.BLOCK_RESULT_PARAM);
                if(blockResult.getTransactionReceipt() != null) {
                    list = (List<Transaction>) mapParamenters.get(Constants.LIST_TRANSACTION_PARAM);
                    String address = mapParamenters.get(Constants.ADDRESS_PARAM).toString();
                    final RealmList<Transaction> transactions = convertListToRealmList(list);
                    blockNumber = blockResult.getTransactionReceipt().getBlockNumber();
                    blockHash = blockResult.getTransactionReceipt().getBlockHash();
                    Transaction tr = findTransaction(txId, transactions);
                    if (tr != null) {
                        transactionInBlockSuccess = true;
                        mCallBack.showSnackBar(mContext.getString(R.string.transaction_in_block));
                        mStorageManager.updateTransactionInBlock(tr, transactions, true, blockNumber.toString(), address);
                        mCallBack.updateListTransactions(true);
                    }
                }else{
                    transaction = (Transaction) mapParamenters.get(Constants.TRANSACTION_PARAM);
                    createAsynkInBlock(transaction, list);
                }
            }
        }catch (Exception e){
            Log.d(TAG, "sendRowInTransaction: EXCEPTION");
            mCallBack.stopProgressDialog();
            e.printStackTrace();
        }
    }


    public Transaction createTransaction(HashMap<String, Object> mapParameters){
        Transaction tr = null;
        String amount = (String) mapParameters.get(Constants.AMOUNT_PARAM);
        String to = (String) mapParameters.get(Constants.TO_PARAM);
        String address = (String) mapParameters.get(Constants.ADDRESS_PARAM);
        String gas = String.valueOf(mapParameters.get(Constants.GAS_PARAM));
        String gasPrice = String.valueOf(mapParameters.get(Constants.GAS_PRICE_PARAM));

        tr = new Transaction(address, to, amount, txId, false, null, null, gas, gasPrice);

        return tr;
    }

    public void manageTransactionsBlock(final HashMap<String, Object> mapParameters/*, EtherAccount account*/){
        Log.d(TAG, "manageTransactionsBlock: START");

//        Realm realm = mStorageManager.getDefaultInstance();
        try {
//            realm.beginTransaction();
//            String address = (String) mapParameters.get(Constants.ADDRESS_PARAM);
//            Transaction tr = realm.copyToRealmOrUpdate(createTransaction(mapParameters));
//            RealmList<Transaction> list = mStorageManager.getTransactionsList(address);
//            list.add(tr);
//            realm.commitTransaction();

            RealmList<Transaction> list = mStorageManager.createAndGetListTransaction(mapParameters, this);
            //----------------------------------------------------------------------------------------------------------------
            //for (int i = 0; i < list.size(); i++) {
            //    Transaction tempTr = list.get(i);
            //   if (!list.get(i).isValidate()) {
            //
            //        createAsynkInBlock(tempTr, list);
//                    sendTransactionInBlock(tempTr.getTxId(), tempTr.getAddress(), list/*, account, mapParameters*/);
             //   }
            //}
            //-----------------------------------------------------------------------------------------------------------------
        }catch (Exception e){
            e.printStackTrace();
        }
//        finally {
//            realm.commitTransaction();
//        }
    }

    public void createAsynkInBlock(Transaction tempTr, List<Transaction> list){
        AsynkTaskTransactionBlockManager asynkBlockTransaction = new AsynkTaskTransactionBlockManager(this);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Constants.TXID_PARAM, tempTr.getTxId());
        map.put(Constants.ADDRESS_PARAM, tempTr.getAddress());
        map.put(Constants.LIST_TRANSACTION_PARAM, list);
        map.put(Constants.TRANSACTION_PARAM, tempTr);
        asynkBlockTransaction.execute(map);
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     *
     * @param password
     * @param to
     * @param nonce
     * @param amount
     * @param gasPrice
     * @param estimateGas
     * @param account
     * @return
     */
    public String generateSignedTransaction(String password, String to, int nonce, BigDecimal amount,  double gasPrice, int estimateGas, EtherAccount account, String data){
        Log.d(TAG, "generateSignedTransaction: START");
        String signedTransaction = null;
        try{
            BigInteger nonceInt = new BigInteger(String.valueOf(nonce));
            BigInteger amountInt = amount.toBigInteger();
            BigInteger gasPriceInt  = new BigInteger(String.valueOf(Math.round(gasPrice)));
            BigInteger gasLimitInt  = new BigInteger(String.valueOf(estimateGas));

            String file = account.getFile();
            //TODO aggiungere selezione account from account manager - eliminare string file

            //WITH PRIVATE KEY
            byte[] privateKey = mEthereum.generatePrivateKey(file, password);
            if(data == null){
                data = "";
            }
            signedTransaction = mEthereum.createSignedTransactionWithPrivateKey(privateKey, to, amountInt, nonceInt, gasPriceInt, gasLimitInt, Util.hexStringToByteArray(data));

        }catch (Exception e){
            Log.d(TAG, "generateSignedTransaction: EXCEPTION: "+e);
            e.printStackTrace();
        }

        return signedTransaction;
    }

    public RealmList<Transaction> convertListToRealmList(List<Transaction> list){
        RealmList<Transaction> realmList = new RealmList<>();
        for(int i=0; i<list.size(); i++){
            realmList.add(list.get(i));
        }
        return realmList;
    }

    public Transaction findTransaction(String txId, List<Transaction> transactions){
        Transaction tr = null;
        try {

            for (int i = 0; i < transactions.size(); i++) {
                if (transactions.get(i).getTxId().equals(txId)) {
                    tr = transactions.get(i);
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tr;
    }

    public BigInteger getGasEstimation() {
        return gasEstimation;
    }

    public BigInteger getNonce() {
        return nonce;
    }

    public BigInteger getGasPrice() {
        return gasPrice;
    }


    //    public void startTimer() {
//        try {
//            TimerTask task = new TimerTask() {
//                @Override
//                public void run() {
//                    elapsed += INTERVAL;
//                    if (elapsed >= TIMEOUT) {
//                        this.cancel();
//                        mCallBack.updateListTransactions(false);
//
//                    }
//                }
//            };
//
//            Timer timer = new Timer();
//            timer.scheduleAtFixedRate(task, INTERVAL, INTERVAL);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    public void startCountDown(){
//
//        final CountDownTimer countDown = new CountDownTimer(WAIT_TIME, 1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                //nothing
//            }
//
//            @Override
//            public void onFinish() {
//                Log.d(TAG, "onFinish counter block transaction");
//                stopBlockTransaction = true;
//                this.cancel(); bhb
//            }
//        }.start();
//    }

//    public List<Transaction> convertRealmListToList(RealmList<Transaction> realmList){
//        List<Transaction> list = new ArrayList<>();
//        try {
//            for (int i = 0; i < realmList.size(); i++) {
//                list.add(realmList.get(i));
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return list;
//    }

}