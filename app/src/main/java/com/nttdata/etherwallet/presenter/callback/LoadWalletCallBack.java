package com.nttdata.etherwallet.presenter.callback;

import com.nttdata.etherwallet.model.EtherAccount;

import java.util.List;

/**
 * Created by Alessandro Faranda Gancio on 08/11/2016.
 */

public interface LoadWalletCallBack {
    void onDoneList(List<EtherAccount> accounts);
    void onDoneOnce(EtherAccount account);
    void onError();
    void manageProgressBar(boolean b);
}
