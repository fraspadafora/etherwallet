package com.nttdata.etherwallet.presenter.http_presenter;

import android.content.Context;

import com.nttdata.etherwallet.manager.async_task.AsincTaskServerCall;
import com.nttdata.etherwallet.manager.data_base.StorageManager;
import android.util.Log;
import com.google.gson.Gson;
import com.nttdata.etherwallet.manager.http_request.VolleyManager;
import com.nttdata.etherwallet.manager.http_request.Web3JManager;
import com.nttdata.etherwallet.presenter.callback.http_callback.SelectFragmentCallBack;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import java.math.BigInteger;

/**
 * Created by Francesco on 17/11/2016.
 */

public class SelectFragmentPresenter {
    private static final String TAG = SelectFragmentPresenter.class.getSimpleName();

    private SelectFragmentCallBack mCallBack;
    private  Context mContext;

    //response parameters
//    private double balance;
    private BigInteger balance;

    private final Gson gson = new Gson();
//    private VolleyManager mVolleyManager;
//    private Web3JManager mWeb3JManager;
//    private StorageManager mStorageManager;

    public SelectFragmentPresenter(Context context, SelectFragmentCallBack callBack) {
//        mVolleyManager = VolleyManager.getInstance(context);
        //mWeb3JManager = Web3JManager.getInstance("http://10.166.30.2:8082/");
//        mWeb3JManager = Web3JManager.getInstance("http://168.128.12.89:8545/");
//        mStorageManager = StorageManager.getInstance(context);
        this.mContext = context;
        this.mCallBack = callBack;
    }

    public void balanceRequest(String address){
        Log.d(TAG, "balanceRequest: START");
        try {
            AsincTaskServerCall asynk = new AsincTaskServerCall(/*mWeb3JManager,*/ address, balance, mContext,/*, mStorageManager,*/ null, mCallBack);
            asynk.execute();
            /*EthGetBalance ethBalance = mWeb3JManager.getBalance(address);
            balance = ethBalance.getBalance();
            mStorageManager.updateBalance(address, balance);
            mCallBack.updateAccountList();*/

        }catch (Exception e){
            Log.d(TAG, "balaceRequest: EXCEPTION");
            e.printStackTrace();
            mCallBack.serverUnreachable();
        }
    }

//    public void balanceRequest(final String address){
//        Log.d(TAG, "balanceRequest: ");
//        VolleyManager.ResponseHandler handler = null;
//        try {
//            handler = new VolleyManager.ResponseHandler() {
//
//                @Override
//                public void success(ResultCode code, String response) {
//                    Log.d(TAG, " balaceRequest success: ");
//
//                    try {
//                        DataResponse dataResponse = gson.fromJson(response, DataResponse.class);
//                        balance = dataResponse.getData().getBalance();
//                        mStorageManager.updateBalance(address, balance);
//                        mCallBack.updateAccountList();
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void failure(ResultCode code, String response, Exception error) {
//                    Log.d(TAG, "balaceRequest failure: ");
//                    balanceRequest(address);
//                }
//            };
//
//            BalanceRequest balance = new BalanceRequest();
//            balance.setAddress(address);
//            String params = gson.toJson(balance);
//            JSONObject request = new JSONObject(params);
//
//            mVolleyManager.generateRequest(Request.Method.POST, UrlServerPath.BALANCE_PATH_HTTP, request, handler);
//
//        }catch (Exception e){
//            Log.d(TAG, "balaceRequest: EXCEPTION");
//            e.printStackTrace();
//        }
//    }

//    public double getBalance() {
//        return balance;
//    }


    public BigInteger getBalance() {
        return balance;
    }
}